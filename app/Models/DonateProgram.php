<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonateProgram extends Model
{
	use HasFactory;

	protected $guarded = [];

	// protected $dates = [
	// protected $casts = [
	// 	'end_at' => 'datetime',
	// ];

	/**
	 * Get all of the receipt_histories for the DonateProgram
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function receipt_histories()
	{
		return $this->hasMany(DonateProgramHistory::class, 'program_id', 'id');
	}
}
