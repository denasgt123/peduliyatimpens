<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
	use HasFactory;

	protected $guarded = [];

	/**
	 * Get the category that owns the PaymentMethod
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(PaymentMethodCategory::class, 'payment_method_category_id', 'id');
	}
}
