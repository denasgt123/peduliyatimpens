<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethodCategory extends Model
{
	use HasFactory;

	protected $guarded = [];

	/**
	 * Get all of the methods for the PaymentMethodCategory
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function methods()
	{
		return $this->hasMany(PaymentMethod::class, 'payment_method_category_id', 'id');
	}
}
