<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonateProgramHistory extends Model
{
	use HasFactory;

	protected $guarded = [];

	/**
	 * Get the program that owns the DonateProgramHistory
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function program()
	{
		return $this->belongsTo(DonateProgram::class, 'program_id');
	}

	/**
	 * Get the paymentMethod that owns the DonateProgramHistory
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function paymentMethod()
	{
		return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
	}

	/**
	 * Get the donatur that owns the DonateProgramHistory
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function donatur()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
