<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
	//tambahkan script di bawah ini
	public function redirectToProvider()
	{
		return Socialite::driver('google')->redirect();
	}


	//tambahkan script di bawah ini 
	public function handleProviderCallback(Request $request)
	{
		try {
			$user_google    = Socialite::driver('google')->user();
			$user           = User::where('email', $user_google->getEmail())->first();

			//jika user ada maka langsung di redirect ke halaman home
			//jika user tidak ada maka simpan ke database
			//$user_google menyimpan data google account seperti email, foto, dsb

			if ($user != null) {
				Auth::login($user, true);
				return redirect()->route('home');
			} else {
				Storage::disk('public_path')->put('img/user/' . $user_google->getEmail() . '.png', file_get_contents($user_google->user['picture']));
				// dd(file_get_contents($user_google->user['picture']));
				// dd($user_google->user['picture']);
				$create = User::Create([
					'email'             => $user_google->getEmail(),
					'name'              => $user_google->getName(),
					'photo'				=> $user_google->getEmail() . '.png',
					'password'          => 0,
					'email_verified_at' => now()
				]);


				Auth::login($create, true);
				return redirect()->route('home');
			}
		} catch (\Exception $e) {
			return redirect()->route('login');
		}
	}
}
