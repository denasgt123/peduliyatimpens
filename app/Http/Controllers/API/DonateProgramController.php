<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DonateProgramResource;
use App\Models\DonateProgram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadFile;
use Illuminate\Support\Str;

class DonateProgramController extends BaseController
{
	use UploadFile;

	public function index()
	{
		$dps = DonateProgram::all();

		return $this->sendResponse(DonateProgramResource::collection($dps), "List Donate Program Berhasil Didapatkan!");
	}

	public function show(DonateProgram $donate_program)
	{
		return $this->sendResponse(new DonateProgramResource($donate_program), "Donate Program $this->found_msg");
	}

	public function store(Request $request)
	{
		$rules = [
			'name'                  => ['required', 'string'],
			'photo'					=> ['required', 'image'],
			'description'           => ['sometimes', 'required', 'string'],
			'donate_goal'           => ['sometimes', 'required', 'string'],
			'current_donate_amount' => ['sometimes', 'required', 'string'],
			'donation_count'        => ['sometimes', 'required', 'string'],
			'end_at'                => ['required', 'string', 'date_format:d-m-Y'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		// $dp     = new DonateProgram($request->only(['name', 'description', 'donate_goal']));
		$dp     = new DonateProgram($request->except(['end_at', 'photo']));

		$end_at = Carbon::createFromFormat('d-m-Y', $request->end_at);
		$dp->end_at = $end_at;
		$dp->program_type = 'temporary';
		$dp->slug = Str::slug($request->name);
		$result = $this->uploadFile($request->photo, 'img/asset/donateprogram');

		if ($result[0]) {
			$dp->photo = $result[1];
		} else {
			return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
		}

		$dp->save();

		return $this->sendResponse(new DonateProgramResource($dp), "Donate Program $this->created_msg");
	}

	public function update(Request $request, DonateProgram $donate_program)
	{
		$rules = [
			'name'                  => ['required', 'string',],
			'photo'					=> ['sometimes', 'required', 'image'],
			'description'           => ['nullable', 'string'],
			'donate_goal'           => ['sometimes', 'required', 'string'],
			'current_donate_amount' => ['sometimes', 'required', 'string'],
			'donation_count'        => ['sometimes', 'required', 'string'],
			'end_at'                => ['required', 'string', 'date_format:d-m-Y'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$end_at = Carbon::createFromFormat('d-m-Y', $request->end_at);

		$donate_program->fill($request->except(['end_at', 'photo']));
		$donate_program->end_at = $end_at;
		$donate_program->slug = Str::slug($request->name);

		if ($request->photo) {
			$result = $this->uploadFile($request->photo, 'img/asset/donateprogram');
			// File::delete(public_path('testjs/'.$file->getFilename()));

			if ($result[0]) {
				// if (file_exists(public_path('img/asset/donateprogram/' . $donate_program->photo))) {
				// File::delete(public_path('img/asset/donateprogram/' . $donate_program->photo));
				// unlink(public_path('img/asset/donateprogram/' . $donate_program->photo));
				// }
				$donate_program->photo = $result[1];
			} else {
				return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
			}
		}

		$donate_program->save();

		return $this->sendResponse(new DonateProgramResource($donate_program), "Donate Program $this->edited_msg");
	}

	public function destroy(DonateProgram $donate_program)
	{
		$donate_program->delete();
		return $this->sendSuccess("Donate Progarm $this->deleted_msg");
	}
}
