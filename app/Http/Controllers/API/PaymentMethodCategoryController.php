<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Resources\PaymentMethodCategoryResource;
use App\Models\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadFile;
use Illuminate\Validation\Rules\Enum;
use App\Enums\PaymentMethodType;
use App\Models\PaymentMethodCategory;

class PaymentMethodCategoryController extends BaseController
{
	use UploadFile;

	public function index()
	{
		$payment_categories = PaymentMethodCategory::all();

		return $this->sendResponse(PaymentMethodCategoryResource::collection($payment_categories), "List Kategori Pembayaran Berhasil Didapatkan!");
	}

	public function show(PaymentMethodCategory $payment_category)
	{
		return $this->sendResponse(new PaymentMethodCategoryResource($payment_category), "Kategori Pembayaran $this->found_msg");
	}

	public function store(Request $request)
	{
		$rules = [
			'name' => ['required', 'string'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$pms = new PaymentMethodCategory($request->all());
		$pms->save();

		return $this->sendResponse(new PaymentMethodCategoryResource($pms), "Kategori Pembayaran $this->created_msg");
	}

	public function update(Request $request, PaymentMethodCategory $payment_category)
	{
		$rules = [
			'name' => ['sometimes', 'required', 'string'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$payment_category->fill($request->all());
		$payment_category->save();

		return $this->sendResponse(new PaymentMethodCategoryResource($payment_category), "Kategori Pembayaran $this->edited_msg");
	}

	public function destroy(PaymentMethodCategory $payment_category)
	{
		$payment_category->delete();
		return $this->sendSuccess("Kategori Pembayaran $this->deleted_msg");
	}
}
