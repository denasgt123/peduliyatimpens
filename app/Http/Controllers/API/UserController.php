<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends BaseController
{
	use UploadFile;
	public function index()
	{
		$users = User::all(['name', 'email', 'phone']);
		$users = User::find(Auth::id())->first();

		return $this->sendResponse(UserResource::collection($users), "List User Berhasil Didapatkan!");
	}

	public function getDetail()
	{
		$user = User::where('id', Auth::id())->first(['photo', 'name', 'bio', 'email', 'phone', 'birth_date', 'email_verified_at']);
		// $user = User::where('api_token', $request->header('token'))->first(['photo', 'name', 'bio', 'email', 'phone', 'birth_date', 'email_verified_at']);

		return $this->sendResponse(new UserResource($user), "Data User $this->found_msg");
	}

	public function checkEmail(Request $request)
	{
		$email = $request->input('email');

		$emailExists = User::where('email', $email)->exists();

		return response()->json(['is_email_exists' => $emailExists]);
	}


	public function update(Request $request)
	{
		$rules = [
			'name'       => ['required', 'string'],
			'photo'      => ['nullable', 'image'],
			'bio'        => ['required', 'string'],
			'birth_date' => ['required', 'string', 'date_format:Y-m-d'],
			'phone'      => ['required', 'string'],
			'email'      => ['required', 'email', Rule::unique('users')->ignore(Auth::id())],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$user = User::find(Auth::id());

		if ($request->photo) {

			if (!$user->_useDefaultPhoto()) {
                File::delete("img/user/$user->photo");
            }

			$result = $this->uploadFile($request->photo, 'img/user');

			if ($result[0]) {
				$user->photo = $result[1];
			} else {
				return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
			}
		}

		$user->fill($request->except(['photo', '_method']));

		$user->save();

		return $this->sendResponse(new UserResource($user), "Data User $this->edited_msg");
	}

	public function resendVerification() {

	}
}
