<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\DonateResource;
use App\Models\DonateProgramHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DonateController extends BaseController
{
	public function index(Request $request)
	{
		$history_q = DonateProgramHistory::query();
		if ($request->program_id) {
			$history_q->whereProgramId($request->program_id);
		}

		$donate_histories = $history_q->get();

		return $this->sendResponse(new DonateResource($donate_histories), "Riwayat Donasi $this->found_msg");
	}

	public function store(Request $request)
	{
		$rules = [
			'is_anonim'         => ['required', 'boolean'],
			'program_id'        => ['required', 'exists:donate_programs,id'],
			// 'user_id'			=> ['required', 'exists:users,id'],
			// 'transaction_id'    => ['required', 'string'],
			// 'payment_method_id' => ['required', 'exists:payment_methods,id'],
			'amount_donation'   => ['required', 'numeric'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$date = Carbon::now()->format('dmYHis');
		$transaction_id = "PYPI-" . $date . "-" . $request->program_id . Auth::id() . rand(0, 99999);

		$data['transaction_details'] = [
			"order_id" => $transaction_id,
			"gross_amount" => $request->amount_donation,
		];

		$response = Http::acceptJson()->withHeaders([
			'Authorization' => 'Basic ' . base64_encode(env('MIDTRANS_SERVER_KEY') . ":"),
			'Content-Type' => 'application/json',
		])->post(env('MIDTRANS_SANDBOX_SNAP_API_URL') . "transactions", $data);
		// dd($response);

		$result = json_decode($response->body());

		if ($result) {
			$history = new DonateProgramHistory($request->all());

			$history->status  = 'menunggu';
			$history->transaction_id  = $transaction_id;
			// $history->status  = 'sukses';
			$history->user_id = Auth::id();
			$history->snap_id = $result->token;

			$history->save();

			return $this->sendResponse(new DonateResource($history), "Donasi $this->created_msg");
		}
	}
}
