<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Resources\PaymentMethodResource;
use App\Models\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadFile;
use Illuminate\Validation\Rules\Enum;
use App\Enums\PaymentMethodType;

class PaymentMethodController extends BaseController
{
	use UploadFile;

	public function index()
	{
		$payment_methods = PaymentMethod::all();

		return $this->sendResponse(PaymentMethodResource::collection($payment_methods), "List Metode Pembayaran Berhasil Didapatkan!");
	}

	public function show(PaymentMethod $payment_method)
	{
		return $this->sendResponse(new PaymentMethodResource($payment_method), "Metode Pembayaran $this->found_msg");
	}

	public function store(Request $request)
	{
		$rules = [
			'name'                  		=> ['required', 'string'],
			'logo'							=> ['required', 'image'],
			'payment_method_category_id'	=> ['required', 'numeric'],
			'fee'           				=> ['required', 'numeric'],
			'fee_type'           			=> ['required', new Enum(PaymentMethodType::class)],
			'is_active'           			=> ['sometimes', 'required', 'numeric'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		$pm = new PaymentMethod($request->except('logo'));

		$result = $this->uploadFile($request->logo, 'img/pembayaran');

		if ($result[0]) {
			$pm->logo = $result[1];
		} else {
			return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
		}

		$pm->save();

		return $this->sendResponse(new PaymentMethodResource($pm), "Metode Pembayaran $this->created_msg");
	}

	public function update(Request $request, PaymentMethod $payment_method)
	{
		$rules = [
			'name'                  		=> ['required', 'string'],
			'logo'							=> ['sometimes', 'required', 'image'],
			'payment_method_category_id'	=> ['required', 'numeric'],
			'fee'           				=> ['required', 'numeric'],
			'fee_type'           			=> ['required', new Enum(PaymentMethodType::class)],
			'is_active'           			=> ['sometimes', 'required', 'numeric'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		if (isset($request->logo)) {
			$payment_method->fill($request->except('logo'));
			if ($request->logo) {
				$result = $this->uploadFile($request->logo, 'img/pembayaran');
				// File::delete(public_path('testjs/'.$file->getFilename()));

				if ($result[0]) {
					// if (file_exists(public_path('img/asset/donateprogram/' . $payment_method->photo))) {
					// File::delete(public_path('img/asset/donateprogram/' . $payment_method->photo));
					// unlink(public_path('img/asset/donateprogram/' . $payment_method->photo));
					// }
					$payment_method->logo = $result[1];
				} else {
					return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
				}
			}
		} else {
			$payment_method->fill($request->all());
		}

		$payment_method->save();

		return $this->sendResponse(new PaymentMethodResource($payment_method), "Metode Pembayaran $this->edited_msg");
	}

	public function destroy(PaymentMethod $payment_method)
	{
		$payment_method->delete();
		return $this->sendSuccess("Metode Pembayaran $this->deleted_msg");
	}
}
