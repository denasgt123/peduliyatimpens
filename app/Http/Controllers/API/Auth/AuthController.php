<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\API\BaseController;
use App\Models\User;
use App\Traits\UploadFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
	use UploadFile;

	public function register(Request $request)
	{

		$rules = [
			'name'       => ['required', 'string'],
			'bio'        => ['nullable', 'string'],
			'email'      => ['required', 'email', 'unique:users,email'],
			'phone'      => ['nullable', 'string'],
			'photo'      => ['sometimes', 'required', 'image', 'max:2048'],
			'birth_date' => ['nullable', 'string', 'date_format:d-m-Y'],
			'password'   => ['required', 'string'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		if ($request->photo != null && $request->birth_date != null) {
			$user  = new User($request->except(['photo', 'birth_date', 'password']));
			// $this->createDirectory('user_profiles');

			$result = $this->uploadFile($request->photo, 'img/user');

			if ($result[0]) {
				$user->photo = $result[1];
			} else {
				return $this->sendError('Ada kesalahan saat menyimpan gambar.', [], 500);
			}
			$birth_date = Carbon::createFromFormat('d-m-Y', $request->birth_date);
			$user->birth_date = $birth_date;
		} elseif($request->birth_date != null) {
			$user  = new User($request->except(['birth_date', 'password']));
			$birth_date = Carbon::createFromFormat('d-m-Y', $request->birth_date);
			$user->birth_date = $birth_date;
		} else {
			$user  = new User($request->except('password'));
		}

		$user->password = Hash::make($request->password);
		$user->save();

		$token = $user->createToken('auth_token')->plainTextToken;

		$user->api_token = $token;
		$user->save();

		$data = [
			'access_token' => $token,
			'token_type'   => 'Bearer'
		];

		return $this->sendResponse($data, "Hi {$user->name}, Welcome To Peduli Yatim PENS!");
	}

	public function login(Request $request)
	{

		// $msg = [
		//     'email.email'    => 'Email anda bukan email yang valid',
		//     'email.required' => 'harap input email',
		//     'email.exists'   => 'Email tidak terdaftar di database.',
		// ];

		$rules = [
			'email'    => ['required', 'email', 'exists:users,email'],
			'password' => ['required', 'string'],
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return $this->sendError('Input tidak sesuai dengan ketentuan.', $validator->errors(), 400);
		}

		if (!Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
			return $this->sendError('Email atau Password Anda Salah!', [], 401);
		}

		$user = User::whereEmail($request->email)->first();

		$token = $user->createToken('auth_token')->plainTextToken;

		$user->api_token = $token;
		$user->save();

		$data = [
			'access_token' => $token,
			'token_type'   => 'Bearer'
		];

		return $this->sendResponse($data, "Hi {$user->name}, Welcome To Peduli Yatim PENS!");
	}

	// method for user logout and delete token
	public function logout(Request $request)
	{
		/** @var \App\Models\User $user **/
		// $user = Auth::user();

		// $user->tokens()->delete();
		$request->user()->currentAccessToken()->delete();

		return $this->sendSuccess('Anda Berhasil Logout!');
	}
}
