<?php

namespace App\Http\Controllers;

use App\Models\DonateProgram;
use App\Models\DonateProgramHistory;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	// dd(Auth::check());
	// 	if (Auth::check() && Auth::user()->email_verified_at == null) {

	// 		return redirect()->route('verification.notice');
	// 	}
	// }

	public function getAllTempPrograms(Request $request = null)
	{
		$now = Carbon::now()->format('Y-m-d H:i:s');

		if (isset($request)) {
			$temp_donate_programs = DonateProgram::whereProgramType('temporary')
				->when($request->filter, function ($q) use ($request) {
					switch ($request->filter) {
						case 'latest':
							$q->orderBy('created_at', 'asc');
							break;

						case 'nearest-deadline':
							$q->orderBy('end_at', 'asc');
							break;

							// case 'least-funded':
							// 	$q->orderBy('end_at', 'desc');
							// 	break;
					}
				})
				->when($request->search_program, function ($q) use ($request) {
					$q->where('name', 'LIKE', "%$request->search_program%");
				})->get();
		} else {
			$temp_donate_programs = DonateProgram::whereProgramType('temporary')->get();
		}

		foreach ($temp_donate_programs as $key => $item) {
			if ($item->end_at <= $now) {
				$temp_donate_programs->forget($key);
				continue;
			}
			$progress = ($item->current_donate_amount / $item->donate_goal) * 100;
			if ($progress <= 100) {
				$item->progress = round($progress, 1);
			} else {
				$item->progress = 100;
			}
		}

		return $temp_donate_programs;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		if (Auth::check() && Auth::user()->email_verified_at == null) {
			// dd('test');
			return redirect()->route('verification.notice');
		}

		$temp_donate_programs = $this->getAllTempPrograms();
		$donate_programs = DonateProgram::whereProgramType('permanent')->get();
		// dd($donate_programs);

		// $now = Carbon::now()->format('Y-m-d H:i:s');

		// foreach ($temp_donate_programs as $key => $item) {
		// 	if ($item->end_at <= $now) {
		// 		$temp_donate_programs->forget($key);
		// 		continue;
		// 	}
		// 	$progress = ($item->current_donate_amount / $item->donate_goal) * 100;
		// 	if ($progress <= 100) {
		// 		$item->progress = round($progress, 1);
		// 	} else {
		// 		$item->progress = 100;
		// 	}
		// }

		return view('home', compact(['temp_donate_programs', 'donate_programs']));
	}

	public function listProgram(Request $request)
	{
		$programs = $this->getAllTempPrograms($request);
		$permanent_programs = DonateProgram::whereProgramType('permanent')->get();

		return view('list-donate-program', compact(['programs', 'permanent_programs']));
	}

	public function financeReport()
	{
		$programs = $this->getAllTempPrograms();



		return view('finance-report', compact('programs'));
	}

	public function donationHistory($program_name)
	{
		$permanent_programs = DonateProgram::whereProgramType('permanent')->get();
		$status = [
			'gagal' => [
				'name'  => 'Batal',
				'color' => 'danger',
			],
			'menunggu' => [
				'name'  => 'Menunggu',
				'color' => 'secondary',
			],
			'sukses' => [
				'name'  => 'Sukses',
				'color' => 'success',
			],
		];

		if ($program_name == 'program-lainnya') {
			$histories = DonateProgramHistory::with('program')->whereHas('program', function ($q) {
				$q->whereProgramType('temporary');
			})->get();
			$program_name = "Program Lainnya";
			$active_filter = 6;
		} else {
			// $program = DonateProgram::first();
			$program = DonateProgram::with(['receipt_histories' => function ($q) {
				$q->orderBy('status', 'desc');
			}])->whereSlug($program_name)->whereProgramType('permanent')->first();

			$histories = $program->receipt_histories;
			$program_name = $program->name;
			$active_filter = $program->id;
		}

		// $programs = $this->getAllTempPrograms();
		// $permanent_programs = DonateProgram::where('program_type', 'permanent')->get();

		foreach ($permanent_programs as $key => $program) {
			switch ($key) {
				case 0:
					$program->shortname = "Infaq Yatim Dhuafa";
					break;
				case 1:
					$program->shortname = "Zakat";
					break;
				case 2:
					$program->shortname = "Waqaf Tanah";
					break;
				case 3:
					$program->shortname = "Infaq Swakelola";
					break;
				case 4:
					$program->shortname = "Modal";
					break;
			}
		}

		// return view('donation-history', compact(['programs', 'permanent_programs']));
		return view('donation-history', compact(['permanent_programs', 'program_name', 'histories', 'status', 'active_filter']));
	}

	public function detailProgram(DonateProgram $program)
	{
		// $program = DonateProgram::whereSlug($slug)->firstOrFail();
		$progress = ($program->current_donate_amount / $program->donate_goal) * 100;
		if ($progress <= 100) {
			$program->progress = round($progress, 1);
		} else {
			$program->progress = 100;
		}

		$histories = $program->receipt_histories;
		// foreach ($program->receipt_histories as $history) {
		// 	dd($history->donatur->name);
		// }

		return view('detail-donate-program', compact(['program', 'histories']));
	}

	public function createDonation(DonateProgram $program)
	{
		$payment_categories = PaymentMethodCategory::with('methods')->get();
		return view('create-donation', compact(['program', 'payment_categories']));
	}

	public function storeDonation(Request $request, DonateProgram $program)
	{
		$request->validate([
			'hide_me' => ['sometimes', 'required', 'boolean'],
			'payment_method' => ['required', 'string'],
			'gross_amount' => ['required', 'numeric'],
			'net_amount' => ['required', 'numeric'],
		]);

		$request->hide_me = $request->hide_me ?? 0;

		$data = [];
		$payment_method = PaymentMethod::where('name', $request->payment_method)->first();
		$transfer_bank = ['bca', 'bni', 'bri'];
		$transfer_wallet = ['gopay', 'shopeepay', 'qris'];
		if (in_array($request->payment_method, $transfer_bank)) {
			$data['payment_type'] = "bank_transfer";
			$data['bank_transfer'] = ["bank" => $request->payment_method];
		} elseif ($request->payment_method == 'mandiri') {
			$data['payment_type'] = "echannel";
			$data['echannel'] = [
				"bill_info1" => "Donasi:",
				"bill_info2" => "Yayasan Peduli Yatim PENS-ITS",
			];
		} elseif ($request->payment_method == 'permata') {
			$data['payment_type'] = "permata";
		} elseif ($request->payment_method == 'gopay') {
			$data['payment_type'] = "gopay";
		} else {
			$data['payment_type'] = "qris";
			$data['qris'] = ["acquirer" => "gopay"];
		}

		$date = Carbon::now()->format('dmYHis');
		$transaction_id = "PYPI-" . $date . "-" . $program->id . Auth::id() . rand(0, 99999);

		$data['transaction_details'] = [
			"order_id" => $transaction_id,
			"gross_amount" => $request->gross_amount,
		];

		$data['items_details'] = [
			"id" => $program->id,
			"price" => $request->net_amount,
			"name" => $program->name,
		];

		$data['customer_details'] = [
			"first_name" => Auth::user()->name,
			"email" => Auth::user()->email,
			"phone" => Auth::user()->phone,
		];

		$response = Http::acceptJson()->withHeaders([
			'Authorization' => 'Basic ' . base64_encode(env('MIDTRANS_SERVER_KEY') . ":"),
			'Content-Type' => 'application/json',
		])->post(env('MIDTRANS_SANDBOX_API_URL') . "charge", $data);
		// dd($response);

		$result = json_decode($response->body());

		if ($result->status_code == 201) {
			$donation_history = new DonateProgramHistory();
			$donation_history->user_id = Auth::id();
			$donation_history->program_id = $program->id;
			$donation_history->transaction_id = $transaction_id;
			$donation_history->payment_method_id = $payment_method->id;
			if ($result->transaction_status == 'pending') {
				$donation_history->status = 'menunggu';
			} elseif ($result->transaction_status == 'settlement') {
				$donation_history->status = 'sukses';
			} else {
				$donation_history->status = 'gagal';
			}
			$donation_history->amount_donation = $request->net_amount;
			$donation_history->is_anonim = $request->hide_me;
			if ($result->payment_type == 'gopay') {
				$donation_history->qris_payment = $result->actions[0]->url;
			}

			$donation_history->save();
			return redirect()->route('detail-donation', $transaction_id);
		}

		// dd("Status Code : " . $result->status_code . " | Transaction Status : " . $result->transaction_status);
	}

	public function detailDonation($order_id)
	{
		$donation_query = DonateProgramHistory::where('transaction_id', $order_id);
		$donation = $donation_query->first();
		$donation_history = $donation_query->first();
		// $donation = $donation_history;
		$response = Http::acceptJson()->withHeaders([
			'Authorization' => 'Basic ' . base64_encode(env('MIDTRANS_SERVER_KEY') . ":"),
			'Content-Type' => 'application/json',
		])->get(env('MIDTRANS_SANDBOX_API_URL') . $donation->transaction_id . "/status");

		$midtrans = json_decode($response->body());
		// dd($midtrans);
		$transfer_va_bank = ['bca', 'bni', 'bri'];
		// dd(in_array($donation->paymentMethod->name, $transfer_va_bank));
		if (in_array($donation->paymentMethod->name, $transfer_va_bank)) {
			$donation->name = "va " . $donation->paymentMethod->name;
			$donation->payment_target = [
				"Nomor Virtual Account" => $midtrans->va_numbers[0]->va_number,
			];
			$end_at = Carbon::parse($midtrans->transaction_time)->addDay(1);
		} elseif ($donation->paymentMethod->name == 'permata') {
			$donation->name = "va " . $donation->paymentMethod->name;
			$donation->payment_target = [
				"Nomor Virtual Account" => $midtrans->permata_va_number,
			];
			$end_at = Carbon::parse($midtrans->transaction_time)->addDay(1);
		} elseif ($donation->paymentMethod->name == 'mandiri') {
			$donation->name = $donation->paymentMethod->name;
			$donation->payment_target = [
				"Bill Key" => $midtrans->bill_key,
				"Kode Biller" => $midtrans->biller_code,
			];
			$end_at = Carbon::parse($midtrans->transaction_time)->addDay(1);
		} elseif ($donation->paymentMethod->name == 'gopay') {
			$donation->name = $donation->paymentMethod->name;
			$donation->payment_target = [
				"QRIS" => $donation->qris_payment,
			];
			$end_at = Carbon::parse($midtrans->transaction_time)->addMinutes(15);
		} else {
			$donation->name = $donation->paymentMethod->name;
		}
		// dd($midtrans->va_numbers[0]->va_number);

		$now = Carbon::now();

		$timelimit = $end_at->diff($now, false)->format('%H:%i:%s');
		$is_timeout = ($end_at->diff($now, false)->invert == 1) ? 0 : 1;
		if ($is_timeout) {
			$donation_history->status = 'gagal';
			$donation->status = 'gagal';
			$donation_history->save();
		}
		// dd($is_timeout);
		// dd($end_at . " | " . $now);
		// dd($end_at->diff($now, false)->invert);
		// dd($midtrans);
		return view('detail-donation', compact(['donation', 'midtrans', 'timelimit', 'is_timeout']));
	}

	public function fixedProgram(DonateProgram $program)
	{
		// dd($program);
		return view('fixed-program', compact('program'));
	}

	// AJAX Handler
	public function checkStatusDonation(DonateProgramHistory $donation)
	{
		if ($donation->status == "menunggu") {
			$response = Http::acceptJson()->withHeaders([
				'Authorization' => 'Basic ' . base64_encode(env('MIDTRANS_SERVER_KEY') . ":"),
				'Content-Type' => 'application/json',
			])->get(env('MIDTRANS_SANDBOX_API_URL') . $donation->transaction_id . "/status");
			$midtrans = json_decode($response->body());

			if ($midtrans->transaction_status == 'settlement') {
				// $donation->amount_donation += 1;
				$donation->status = "sukses";
				$donation->save();

				$donate_program = DonateProgram::where('id', $donation->program_id)->first();
				$donate_program->donation_count += 1;
				$donate_program->current_donate_amount += $donation->amount_donation;
				$donate_program->save();

				return "sukses";
				// return redirect()->route('detail-donation', $donation->transaction_id);
			} else {
				
			}
		}
	}
}
