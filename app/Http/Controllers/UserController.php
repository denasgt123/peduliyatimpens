<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;
use App\Traits\UploadFile;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
	use UploadFile;

	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		return view('edit-profile');
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(Request $request)
	{
		$request->validate([
			'name'			=> ['required', 'string', 'max:255'],
			'bio'			=> ['sometimes', 'required', 'string', 'max:400'],
			'birth_date'	=> ['sometimes', 'required', 'string'],
			'phone'			=> ['sometimes', 'required', 'numeric'],
			'hide_me'		=> ['sometimes', 'required', 'boolean'],
			'whatsapp'		=> ['sometimes', 'required', 'boolean'],
			'email'			=> ['required', Rule::unique('users')->ignore(Auth::user()->id), 'email:rfc,dns'],
			// 'photo' => ['sometimes', 'required', 'image', 'max:10240'],
			// 'gender' => 'required',
			// 'location' => 'required',
			// 'username' => ['required', 'regex:/^\S*$/u', 'unique:users'],
			// 'email' => ['required', 'unique:users', 'email:rfc,dns'],
			// 'password' => ['required', 'confirmed'],
		]);

		$hide_me = $request->hide_me ? $request->hide_me : 0;
		$whatsapp = $request->whatsapp ? $request->whatsapp : 0;

		$user = User::find(Auth::id());

		if ($user->email != $request->email) {
			// dd('yes');
			$user->email_verified_at = null;
		}

		$user->fill($request->all());
		$user->hide_me = $hide_me;
		$user->whatsapp = $whatsapp;

		$user->save();

		Alert::success('Berhasil', 'Data diri berhasi dirubah');

		return back();
		// dd($hide_me);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function updatePhoto(Request $request)
	{
		$request->validate([
			'photo' => ['sometimes', 'required', 'image', 'max:10240'],
		]);

		$user = User::find(Auth::id());

		$result = $this->uploadFile($request->photo, 'img/user');

		if ($result[0]) {
			$user->photo = $result[1];
		} else {
			Alert::toast('Ada kesalahan saat menyimpan gambar!', 'error');
			return back();
		}

		$user->save();

		Alert::toast('Foto Profil berhasil dirubah!', 'success');
		return back();
	}

	public function deletePhoto()
	{
		$user = User::find(Auth::id());
		// dd($user->photo);
		if ($user->photo != 'user.png' && File::exists(public_path('img/user/' . $user->photo))) {
			File::delete(public_path('img/user/' . $user->photo));
			$user->photo = 'user.png';
			$user->save();

			Alert::toast('Berhasil menghapus foto profil anda!', 'success');

			return back();
		} elseif ($user->photo == 'user.png') {
			Alert::warning('Maaf', 'Anda tidak bisa menghapus foto profil karena masih menggunakan foto bawaan!');

			return back();
		} else {
			dd('File not found');
		}
	}

	public function changePassword(Request $request)
	{
		# Validation
		$request->validate([
			'old_password' => 'required',
			'new_password' => 'required|confirmed',
		]);

		# Match The Old Password
		if (!Hash::check($request->old_password, auth()->user()->password)) {
			Alert::error('Gagal', 'Kata Sandi lama tidak cocok!');
			return back();
		}

		# Update the new Password
		User::whereId(auth()->user()->id)->update([
			'password' => Hash::make($request->new_password)
		]);

		Alert::toast('Kata Sandi berhasil dirubah!', 'success');

		return back();
	}

	public function setPassword(Request $request)
	{
		# Validation
		$request->validate([
			'new_password' => 'required|confirmed',
		]);

		# Update the new Password
		User::whereId(auth()->user()->id)->update([
			'password' => Hash::make($request->new_password)
		]);

		Alert::toast('Kata Sandi Baru berhasil diatur!', 'success');

		return back();
	}
}
