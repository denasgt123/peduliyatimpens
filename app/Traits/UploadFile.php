<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Buglinjo\LaravelWebp\Facades\Webp;
use Illuminate\Support\Facades\File;

trait UploadFile
{

    /**
     * Upload file using storeAs() function. Recommend for saving file in storage's symlink folder.
     *
     * @param  Illuminate\Support\Facades\Request::File $file - the uploaded file (e.g., $request->thumbnail)
     * @param  string $where - path you want to put the file at (e.g., "public/photos")
     * @param  string $identifier - custom identifier, adding custom string in front of the filename, required if this function is called twice in the same code to prevent generating duplicate filenames. (example, adding "thumbnail" will make generated file name like "thumbnail_filename_time().jpg")
     * @return array [$path, $savedFileName]
     */
    protected static function uploadFile($file, $where, $identifier = null)
    {
        $filenameWithExt = $file->getClientOriginalName();
        $filename        = Str::slug(pathinfo($filenameWithExt, PATHINFO_FILENAME));
        $extension       = $file->getClientOriginalExtension();
        
        if($identifier) {
            $savedFileName = $identifier .'_'. $filename . '_' . time()  . '.' . $extension;
        } else {
            $savedFileName = $filename . '_' . time()  . '.' . $extension;
        }

        $path = $file->storeAs($where, $savedFileName, ['disk' => 'public_path']);
        
        return [$path, $savedFileName];
    }

    /**
     * Creating Directory if not exist using File::isDirectory() & File::makeDirectory() function. this function will creating the folder only if the folder does not exist.
     *
     * @param  string $path - path of the folder, it will use public_path() (e.g., "public/photos")
     * @param  string $identifier - custom identifier, adding custom string in front of the filename, required if this function is called twice in the same code to prevent generating duplicate filenames. (example, adding "thumbnail" will make generated file name like "thumbnail_filename_time().jpg")
     * @return array [$path, $savedFileName]
     */
    public function createDirectory($path)
    {
        $path = public_path("storage/$path");

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }
}
