<?php

namespace App\Enums;

enum PaymentMethodType: string
{
	case FLATPPN = 'flat_ppn';
	case FLAT = 'flat';
	case PERCENTAGE = 'percentage';
}
