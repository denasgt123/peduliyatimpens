<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GoogleController;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
	return redirect()->route('home');
	// return redirect()->route('construct');
});

Auth::routes(['verify' => true]);

Route::get('/auth/redirect', [GoogleController::class, 'redirectToProvider'])->name('google');
Route::get('/auth/callback', [GoogleController::class, 'handleProviderCallback']);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/list-program', [HomeController::class, 'listProgram'])->name('list-program');
Route::get('/finance-report', [HomeController::class, 'financeReport'])->name('finance-report');
Route::get('/fixed-program/{program:slug}', [HomeController::class, 'fixedProgram'])->name('fixed-program');
Route::get('/detail-program/{program:slug}', [HomeController::class, 'detailProgram'])->name('detail-program');
// Route::get('/infaq-yatim-dhuafa', [HomeController::class, 'programYatimDhuafa'])->name('infaq-yatim-dhuafa');
// Route::get('/zakat', [HomeController::class, 'programZakat'])->name('zakat');
// Route::get('/waqaf-tanah', [HomeController::class, 'programWaqafTanah'])->name('waqaf-tanah');
// Route::get('/infaq-swakelola', [HomeController::class, 'programInfaqSwakelola'])->name('infaq-swakelola');
// Route::get('/modal', [HomeController::class, 'programModal'])->name('modal');

Route::middleware(['auth', 'verified'])->group(function () {
	Route::get('/edit-profile', [UserController::class, 'index'])->name('edit-profile');
	Route::put('/edit-profile', [UserController::class, 'update'])->name('update-profile');
	Route::put('/edit-profpic', [UserController::class, 'updatePhoto'])->name('update-profpic');
	Route::delete('/delete-profpic', [UserController::class, 'deletePhoto'])->name('delete-profpic');
	Route::put('/change-password', [UserController::class, 'changePassword'])->name('change-password');
	Route::put('/set-password', [UserController::class, 'setPassword'])->name('set-password');

	Route::get('/create-donation/{program:slug}', [HomeController::class, 'createDonation'])->name('create-donation');
	Route::post('/store-donation/{program:slug}', [HomeController::class, 'storeDonation'])->name('store-donation');
	Route::get('/donation-history/{program_name}', [HomeController::class, 'donationHistory'])->name('donation-history');
	Route::get('/detail-donation/{order_id}', [HomeController::class, 'detailDonation'])->name('detail-donation');

	Route::get('/check-status/{donation:transaction_id}', [HomeController::class, 'checkStatusDonation'])->name('check-status');
});

Route::get('/coming-soon', function () {
	return view('construct');
})->name('construct');

// Clear application cache:
Route::get('/clear-cache', function () {
	Artisan::call('cache:clear');
	return 'Application cache has been cleared';
});

//Clear route cache:
Route::get('/clear-route', function () {
	Artisan::call('route:cache');
	return 'Routes cache has been cleared';
});

//Clear config cache:
Route::get('/clear-config', function () {
	Artisan::call('config:cache');
	return 'Config cache has been cleared';
});

// Clear view cache:
Route::get('/clear-view', function () {
	Artisan::call('view:clear');
	return 'View cache has been cleared';
});
