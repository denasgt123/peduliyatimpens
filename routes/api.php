<?php

use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\DonateController;
use App\Http\Controllers\API\DonateProgramController;
use App\Http\Controllers\API\PaymentMethodCategoryController;
use App\Http\Controllers\API\PaymentMethodController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/is-email-exist', [UserController::class, 'checkEmail']);

// Route::prefix('users')->group(function () {
// 	Route::get('/{user_id}', [UserController::class, 'getDetail']);
// });

Route::middleware('auth:sanctum')->group(function () {
	Route::post('/logout', [AuthController::class, 'logout']);
	// Route::apiResource('users', UserController::class)->only('update');
	// API USERS
	Route::prefix('users')->group(function () {
		Route::get('/me', [UserController::class, 'getDetail']);
		Route::put('/me', [UserController::class, 'update']);
		// Route::get('/me', [UserController::class, 'getDetail']);
	});

	Route::prefix('donate-programs')->group(function() {
		Route::post('/donate', [DonateController::class, 'store']);
		Route::get('/donation-histories', [DonateController::class, 'index']);
	});
});

Route::apiResource('users', UserController::class)->only('index');

// API DONATE PROGRAM
Route::apiResource('donate-programs', DonateProgramController::class);
Route::apiResource('payment-methods', PaymentMethodController::class);
Route::apiResource('payment-categories', PaymentMethodCategoryController::class);