<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Peduli Yatim PENS') }}</title>

    {{-- Icon --}}
    <link rel="icon" sizes="512x512" href="{{ asset('img/logo.png') }}">

    {{-- Style --}}
    @include('layouts.style')
    @stack('style')
</head>

<body class="min-vh-100">
    @yield('modal')
    @yield('content')
	<form action="" method="POST" id="deleteForm" class="d-none form-delete">
		@csrf
		@method('delete')
	</form>

    {{-- Script --}}
    @include('layouts.script')
    <script src="{{ asset('js/jquery-3.7.0.min.js') }}"></script>

    @stack('script')
</body>

</html>
