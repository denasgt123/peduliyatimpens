{{-- Start Header Navbar Section --}}
<div class="navbar sticky-top p-0 mb-4 shadow-sm">
    <div class="container d-flex align-self-center">
        <a href="{{ route('home') }}" class="d-flex align-items-center text-decoration-none text-dark">
            <img class="nav-logo" src="{{ asset('img/logo.png') }}" alt="logo">
            <h6 class="ms-3 mb-0 fw-bolder fs-6">Peduli Yatim PENS-ITS</h6>
        </a>
        <div class="d-flex align-items-center justify-content-between h-100">
            <a href="{{ route('home') }}"
                class="d-flex align-items-center justify-content-center h-100 px-3 text-decoration-none text-dark {{ Route::is('home') ? 'active-nav-menu' : '' }}">
                <img src="{{ Route::is('home') ? asset('img/Icon Active/home.svg') : asset('img/Icon/home.svg') }}"
                    alt="icon">
                <small class="ms-2 mb-0 fw-semibold">Dashboard</small>
            </a>
            <a href="{{ route('list-program') }}"
                class="d-flex align-items-center justify-content-center h-100 px-3 text-decoration-none text-dark {{ Route::is('list-program') ? 'active-nav-menu' : '' }}">
                <img src="{{ Route::is('list-program') ? asset('img/Icon Active/donateprogram.svg') : asset('img/Icon/donateprogram.svg') }}"
                    alt="icon">
                <small class="ms-2 mb-0 fw-semibold">Program Donasi</small>
            </a>
            <a href="{{ route('finance-report') }}"
                class="d-flex align-items-center justify-content-center h-100 px-3 text-decoration-none text-dark {{ Route::is('finance-report') ? 'active-nav-menu' : '' }}">
                <img src="{{ Route::is('finance-report') ? asset('img/Icon Active/chart.svg') : asset('img/Icon/chart.svg') }}"
                    alt="icon">
                <small class="ms-2 mb-0 fw-semibold">Laporan Keuangan</small>
            </a>
            <a href="{{ route('donation-history', 'infaq-anak-yatim-piatu-dan-dhuafa') }}"
                class="d-flex align-items-center justify-content-center h-100 px-3 text-decoration-none text-dark {{ Route::is('donation-history') ? 'active-nav-menu' : '' }}">
                <img src="{{ Route::is('donation-history') ? asset('img/Icon Active/receipt.svg') : asset('img/Icon/receipt.svg') }}"
                    alt="icon">
                <small class="ms-2 mb-0 fw-semibold">Riwayat</small>
            </a>
        </div>
        @auth
            <div class="d-flex align-items-center justify-content-center h-100">
                {{-- <img src="{{ asset('img/Icon/Bell.svg') }}" alt="icon" style="cursor: pointer;"> --}}
                <div class="dropdown">
                    <div class="d-flex justify-content-center align-items-center ms-1 dropdown-toggle"
                        data-bs-toggle="dropdown" aria-expanded="false" style="cursor: pointer;">
                        <div class="mx-2 rounded-circle overflow-hidden nav-prof-pic-container">
                            <img src="{{ asset('img/user/' . Auth::user()->photo) }}" alt="user" class="nav-prof-pic">
                        </div>
                        <small class="fw-semibold mb-0">{{ strstr(Auth::user()->name . ' ', ' ', true) }}</small>
                    </div>
                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('edit-profile') }}">
                            <i class="fa-solid fa-gear pe-2"></i> Pengaturan
                        </a>
                        <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa-solid fa-right-from-bracket pe-2"></i> Keluar
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        @else
            <div class="d-flex align-items-center justify-content-center h-100">
                <a href="{{ route('login') }}"
                    class="text-decoration-none text-dark py-2 px-3 btn btn-success rounded-pill me-1 fw-semibold text-white">Masuk</a>
                <a href="{{ route('register') }}"
                    class="text-decoration-none text-dark py-2 px-3 btn btn-outline-success rounded-pill ms-1 fw-semibold">Daftar</a>
            </div>
        @endauth
    </div>
</div>
{{-- End Header Navbar Section --}}
