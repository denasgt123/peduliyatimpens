<!-- Fonts -->
{{-- <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet"> --}}

{{-- Bootstrap Style --}}
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
<link rel="stylesheet" href="{{ asset('css/bootstrap5.3.0.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/fontawesome6.4.0.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/style.css') }}" />

{{-- <style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,700;0,800;1,400&display=swap');

    * {
        font-family: 'Open Sans', sans-serif;
    }
</style> --}}