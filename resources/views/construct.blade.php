<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Segera Hadir</title>

    {{-- Icon --}}
    <link rel="icon" sizes="512x512" href="{{ asset('img/Icon Active/gear-solid1.svg') }}">

    <link rel="stylesheet" href="{{ asset('css/bootstrap5.3.0.min.css') }}">

    <style>
        @font-face {
            font-family: Chewy;
            src: url(font/Chewy/Chewy-Regular.ttf);
        }

        * {
            font-family: Chewy;
        }

        html,
        body {
            background-color: #F6F7F7;
            height: 100%;
        }

        .title {
            font-size: 4rem;
            color: #3f3d56;
        }

        .img-title {
            max-height: 25em;
            width: auto;
        }

        .text-green {
            color: #006934;
        }

        @media (min-width: 768px) and (max-width: 991.98px) {
            .title {
                font-size: 3.5rem;
            }

            .img-title {
                max-height: 20em;
                width: auto;
            }
        }

        @media (max-width: 767.98px) {
            .title {
                font-size: 2.5rem;
            }

            .img-title {
                max-height: 10em;
                width: auto;
            }
        }
    </style>
</head>

<body class="d-flex justify-content-center align-items-center">
    <div>
        <img src="{{ asset('img/asset/under-construction/undraw_engineering_team_a7n2.svg') }}" alt="Ilustrasi"
            class="img-title">
        <h1 class="title text-center mt-5">SEGERA <span class="text-green">HADIR</span></h1>
    </div>
</body>

</html>
