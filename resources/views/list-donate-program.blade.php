@extends('layouts.app')

@push('style')
    <style>
        .header {
            min-height: 20rem;
        }

        .program-item {
            height: 11.5rem;
        }

        .img-program {
            height: 100%;
            width: 20%;
            object-fit: cover;
            object-position: center;
        }

        .desc-program {
            height: 100%;
            width: 80%;
        }

        .bg-donation {
            background-color: #E8F6ED;
            color: #007139;
        }

        .dropdown-item.active,
        .dropdown-item:active {
            background-color: #E8F6ED;
        }

        .filter-item {
            width: 16.5rem;
            cursor: pointer;
        }

        .filter-item:hover {
            background-color: #f8f9fa;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')
    {{-- Start Content Section --}}
    <div class="container vw-100">
        {{-- Start Header Section --}}
        <section class="w-100 position-relative overflow-hidden header">
            <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle "
                src="{{ asset('img/asset/home/bg atas.png') }}" alt="Background Img" style="object-fit: cover;">
            <div
                class="position-absolute top-50 start-50 translate-middle px-5 row justify-content-center align-items-center w-100">
                <div class="col-6">
                    <h3 class="fw-semibold">
                        <span class="border-success border-3 border-bottom">
                            Daftar Program Donasi
                        </span>
                    </h3>
                    <h6 class="mb-0 mt-4 fw-semibold">
                        Di sini, Anda dapat mencari dan menemukan berbagai program donasi yang diselenggarakan oleh Yayasan
                        Peduli Yatim PENS-ITS. Setiap donasi Anda memiliki dampak yang luar biasa untuk kami.
                    </h6>
                </div>
                <div class="col-2 offset-1 px-0">
                    <img class="w-100 h-100" src="{{ asset('img/logo.png') }}" alt="logo" style="object-fit: contain;">
                </div>
            </div>
        </section>
        {{-- End Header Section --}}

        {{-- Start Permanent Donate Program Section --}}
        <div class="card border-0 mt-4">
            <div class="row g-0 px-2 py-3">
                <a href="{{ route('fixed-program', $permanent_programs[0]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Infaq Yatim.svg') }}" alt="">
                    <b class="text-program-permanent">Infaq <br>Yatim Dhuafa</b>
                </a>
                <a href="{{ route('fixed-program', $permanent_programs[1]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Zakat.svg') }}" alt="">
                    <b class="text-program-permanent">Zakat</b>
                </a>
                <a href="{{ route('fixed-program', $permanent_programs[2]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Waqaf.svg') }}" alt="">
                    <b class="text-program-permanent">Waqaf Tanah</b>
                </a>
                <a href="{{ route('fixed-program', $permanent_programs[3]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Swakelola.svg') }}" alt="">
                    <b class="text-program-permanent">Infaq Swakelola</b>
                </a>
                <a href="{{ route('fixed-program', $permanent_programs[4]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Modal.svg') }}" alt="">
                    <b class="text-program-permanent">Modal</b>
                </a>
            </div>
        </div>
        {{-- End Permanent Donate Program Section --}}

        {{-- Start Temporarerily Donate Program Section --}}
        <div class="card border-0 mt-4 mb-4">
            <div class="px-4 py-4">
                <form action="{{ route('list-program') }}" method="GET" id="searchProgramForm"
                    class="w-100 d-flex flex-wrap">
                    <input type="hidden" id="filter-input" name="filter">
                    <div class="dropdown pe-4 border-success border-end border-1">
                        <button class="btn btn-success rounded-pill px-4" type="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="fa-solid fa-filter"></i> Filter
                        </button>
                        <ul class="dropdown-menu px-2">
                            {{-- @dd(request()->get('filter')) --}}
                            <li><small class="filter-item d-block rounded-pill px-3 my-2 text-success fw-semibold"
                                    data-filter="latest">Program
                                    Terbaru</small></li>
                            <li><small class="filter-item d-block rounded-pill px-3 my-2 text-success fw-semibold"
                                    data-filter="nearest-deadline">Program
                                    Tenggat Waktu Terdekat</small></li>
                            {{-- <li><small class="filter-item d-block rounded-pill px-3 my-2 text-success fw-semibold bg-donation">Paling
                                    Sedikit Terdanai</small></li> --}}
                        </ul>
                    </div>
                    <input class="col form-control rounded-pill ms-4 ps-4 border-1 border-success" type="search"
                        name="search_program" id="searchProgramInput" placeholder="Cari Program....">
                </form>

                <div class="pt-4 px-3">
                    <h4 class="fw-semibold mb-4 mt-3">List Program Donasi</h4>

                    @if (request()->get('filter') != null || request()->get('search_program') != null)
                        @php
                            $filter = request()->get('filter');
                            $filter_msg = $filter != null && $filter == 'latest' ? 'Filter: "Program Terbaru"' : 'Filter: "Program Tenggat Waktu Terdekat"';
                            $search_msg = request()->get('search_program') != null ? ' & Kata Kunci: "' . request()->get('search_program') . '"' : '';
                            $msg = $filter_msg . $search_msg;
                        @endphp
                        <h6 class="my-3">Hasil Dari: <span class="fw-semibold">{{ $msg }}</span></h6>
                    @endif

                    @forelse ($programs as $program)
                        <a href="{{ route('detail-program', $program->slug) }}" class="text-decoration-none text-dark">
                            <div
                                class="shadow-sm border-1 w-100 mb-3 d-flex flex-wrap program-item rounded overflow-hidden border">
                                <img class="img-program flex-shrink-1"
                                    src="{{ asset('img/asset/donateprogram/' . $program->photo) }}" alt="img">
                                <div class="desc-program py-3 ps-3 pe-4 text-truncate">
                                    <small class="text-secondary">
                                        {{ Carbon\Carbon::parse($program->created_at) }}
                                    </small>
                                    <h5 class="fw-semibold mb-2	">
                                        {{ $program->name }}
                                    </h5>
                                    <small class="text-truncate">
                                        {{ $program->description }}
                                    </small>
                                    <div class="w-100">
                                        <small class="text-secondary-emphasis fw-semibold" style="font-size: 0.65rem;">Dana
                                            terkumpul</small>
                                        <div class="progress" role="progressbar" aria-label="Basic example"
                                            aria-valuenow="{{ $program->progress }}" aria-valuemin="0" aria-valuemax="100"
                                            style="height: 7px;">
                                            <div class="progress-bar bg-success" style="width: {{ $program->progress }}%">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center mt-3">
                                            <span class="badge bg-donation rounded-pill px-4 py-1 fw-bold">
                                                Rp.
                                                {{ number_format($program->current_donate_amount, 0, ',', '.') }},-
                                            </span>
                                            <span class="badge text-dark fw-semibold">
                                                {{ Carbon\Carbon::parse($program->end_at)->diff(Carbon\Carbon::now())->days }}
                                                hari lagi
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @empty
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center pt-5">
                            <div class="d-flex flex-column justify-content-between">
                                <img src="{{ asset('img/asset/home/No Data Illustration.svg') }}" alt="img"
                                    style="height: 15rem; object-postion: center; object-fit: contain;">
                                <h5 class="mb-5 fw-semibold mt-5">Maaf, sementara belum ada program donasi saat ini</h5>
                            </div>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
        {{-- End Temporarerily Donate Program Section --}}
    </div>
    {{-- End Content Section --}}
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            const form = $("#searchProgramForm");
            $("small[data-filter]").click(function(e) {
                e.preventDefault();

                $('#filter-input').val($(this).data('filter'));
                form.submit();
            });
            $('#searchProgramInput').change(function(e) {
                e.preventDefault();
                form.submit();
            });
        });
    </script>
@endpush
