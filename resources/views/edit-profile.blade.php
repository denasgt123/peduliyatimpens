@extends('layouts.app')

@push('style')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
    <style>
    </style>
@endpush

@section('modal')
    {{-- Start change password modal --}}
    <div class="modal fade" id="changePasswordModal" tabindex="-2" aria-labelledby="changePasswordModal" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered ">
            <div class="modal-content">
                <form action="{{ route('change-password') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="editProfileModalLabel">Ubah Kata Sandi</h1>
                        <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                            aria-label="Close"><i class="fa-solid fa-xmark text-dark fs-5"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('old_password') is-invalid @enderror">
                                <input type="password" class="form-control @error('old_password') is-invalid @enderror"
                                    id="oldPassword" placeholder="********" name="old_password">
                                <label for="oldPassword">Kata sandi lama <span class="text-danger">*</span></label>
                            </div>
                            @error('old_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password') is-invalid @enderror">
                                <input type="password" class="form-control @error('new_password') is-invalid @enderror"
                                    id="newPassword" placeholder="********" name="new_password">
                                <label for="newPassword">Kata sandi baru <span class="text-danger">*</span></label>
                            </div>
                            @error('new_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password_confirmation') is-invalid @enderror">
                                <input type="password"
                                    class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                    id="confirmNewPassword" placeholder="********" name="new_password_confirmation">
                                <label for="confirmNewPassword">Konfirmasi kata sandi<span
                                        class="text-danger">*</span></label>
                            </div>
                            @error('new_password_confirmation')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End change password modal --}}

    {{-- Start Set password modal --}}
    <div class="modal fade" id="SetPasswordModal" tabindex="-2" aria-labelledby="SetPasswordModal" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered ">
            <div class="modal-content">
                <form action="{{ route('set-password') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="editProfileModalLabel">Atur Kata Sandi</h1>
                        <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                            aria-label="Close"><i class="fa-solid fa-xmark text-dark fs-5"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password') is-invalid @enderror">
                                <input type="password" class="form-control @error('new_password') is-invalid @enderror"
                                    id="newPassword" placeholder="********" name="new_password">
                                <label for="newPassword">Kata sandi baru <span class="text-danger">*</span></label>
                            </div>
                            @error('new_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password_confirmation') is-invalid @enderror">
                                <input type="password"
                                    class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                    id="confirmNewPassword" placeholder="********" name="new_password_confirmation">
                                <label for="confirmNewPassword">Konfirmasi kata sandi<span
                                        class="text-danger">*</span></label>
                            </div>
                            @error('new_password_confirmation')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End Set password modal --}}

    {{-- @if (session('changePassword'))
		<div class="alert alert-success" role="alert">
			{{ session('changePassword') }}
		</div>
	@elseif (session('error'))
		<div class="alert alert-danger" role="alert">
			{{ session('error') }}
		</div>
	@endif --}}
@endsection

@section('content')
    @include('layouts.navbar')
    <div class="container h-100 min-w-100">
        <div class="card p-3" style="min-height: 80vh;">
            <div class="card-head">
                <h3 class="fw-semibold ms-5 mt-4">Pengaturan Profil</h3>
            </div>
            <div class="card-body">
                <div class="row w-100 h-100 px-4">
                    <div class="col-3">
                        <div class="card shadow-sm border-rounded p-3">
                            <div class="card border-rounded border-0 w-100">
                                <div class="img-container">
                                    <img src="{{ asset('img/user/' . Auth::user()->photo) }}" alt="User"
                                        class="img-prof">
                                </div>
                            </div>
                            <form action="{{ route('update-profpic') }}" method="POST" enctype="multipart/form-data"
                                id="changeProfPicForm">
                                @method('PUT')
                                @csrf
                                <div class="btn btn-sm btn-outline-secondary w-100 rounded d-flex align-items-center justify-content-center py-2 mt-3"
                                    style="cursor: pointer;" onclick="this.nextElementSibling.click()">
                                    <p class="fw-semibold mb-0">Pilih Foto</p>
                                </div>
                                <input class="d-none" type="file" name="photo" id="inputProfPhoto"
                                    onchange="document.getElementById('changeProfPicForm').submit()">
                            </form>
                            <div data-url="{{ route('delete-profpic') }}"
                                class="btn btn-sm btn-outline-danger w-100 rounded d-flex align-items-center justify-content-center py-2 my-3"
                                style="cursor: pointer;" onclick="removePhoto(this)">
                                <p class="fw-semibold mb-0">Hapus Foto</p>
                            </div>
                            <small class="text-wrap text-center text-lg-start">Besar file: maksimum 10.000.000 bytes (10
                                Megabytes). Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</small>
                        </div>
                        @if (Auth::user()->password == 0)
                            <div class="btn btn-sm btn-outline-success w-100 rounded d-flex align-items-center justify-content-center py-2 my-3"
                                data-bs-toggle="modal" data-bs-target="#SetPasswordModal">
                                <p class="fw-semibold mb-0">Atur Kata Sandi</p>
                            </div>
                        @else
                            <div class="btn btn-sm btn-outline-secondary w-100 rounded d-flex align-items-center justify-content-center py-2 my-3"
                                data-bs-toggle="modal" data-bs-target="#changePasswordModal">
                                <p class="fw-semibold mb-0">Ubah Kata Sandi</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-7 ps-5">
                        <form action="{{ route('update-profile') }}" method="POST" id="editProfileForm">
                            @csrf
                            @method('PUT')
                            <h6 class="text-secondary fw-semibold">Ubah Profil Diri</h6>
                            <table cellspacing="0" cellpadding="0" class="mt-3 w-100" style="border: none;">
                                <tr>
                                    <td class="first-col h-100">
                                        <label for="inputName" class="form-label mb-0 fs-6">Nama</label>
                                    </td>
                                    <td class="mid-col fw-semibold">
                                        :
                                    </td>
                                    <td class="last-col">
                                        <input type="text"
                                            class="form-control form-control-sm @error('name') is-invalid @enderror"
                                            id="inputName" placeholder="Jhony Sin" name="name"
                                            value="{{ Auth::user()->name }}">

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="first-col h-100">
                                        <label for="inputBoD" class="form-label mb-0">Tanggal lahir</label>
                                    </td>
                                    <td class="mid-col fw-semibold">
                                        :
                                    </td>
                                    <td class="last-col">
                                        <input type="date"
                                            class="form-control form-control-sm @error('birth_date') is-invalid @enderror"
                                            id="inputBoD" name="birth_date"
                                            value="{{ \Carbon\Carbon::parse(Auth::user()->birth_date)->format('Y-m-d') }}">

                                        @error('birth_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="first-col h-100">
                                        <label for="inputBio" class="form-label mb-0">Bio</label>
                                    </td>
                                    <td class="mid-col fw-semibold">
                                        :
                                    </td>
                                    <td class="last-col">
                                        <textarea class="form-control form-control-sm @error('bio') is-invalid @enderror"
                                            placeholder="Masukan bio anda disini" id="inputBio" name="bio" rows="4">{{ Auth::user()->bio }}</textarea>
                                        @error('bio')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                            </table>
                            <h6 class="text-secondary fw-semibold pt-4">Ubah Kontak</h6>
                            <table cellspacing="0" cellpadding="0" class="mt-3 w-100" style="border: none;">
                                <tr>
                                    <td class="first-col h-100">
                                        <label for="inputTelp" class="form-label mb-0 fs-6">Telepon</label>
                                    </td>
                                    <td class="mid-col fw-semibold">
                                        :
                                    </td>
                                    <td class="last-col">
                                        <input type="number"
                                            class="form-control form-control-sm @error('phone') is-invalid @enderror"
                                            id="inputTelp" placeholder="0853xxxxxxxx" name="phone"
                                            value="{{ Auth::user()->phone == null ? '' : Auth::user()->phone }}">

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td class="first-col h-100">
                                        <label for="inputEmail" class="form-label mb-0">Email</label>
                                    </td>
                                    <td class="mid-col fw-semibold">
                                        :
                                    </td>
                                    <td class="last-col">
                                        <input type="email"
                                            class="form-control form-control-sm @error('email') is-invalid @enderror"
                                            id="inputEmail" placeholder="user@gmail.com" name="email"
                                            value="{{ Auth::user()->email }}">
                                        @error('email')
                                            <span class="invalid-feedback mb-0" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                            </table>
                            <h6 class="text-secondary fw-semibold pt-4">Ubah Pengaturan</h6>
                            <div class="mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" role="switch" id="anonimMode"
                                        name="hide_me" value="1" {{ Auth::user()->hide_me ? 'checked' : '' }}>
                                    <label class="form-check-label" for="anonimMode">Sembunyikan saya</label>
                                </div>
                                {{-- <div class="form-check form-switch mt-2">
                                    <input class="form-check-input" type="checkbox" role="switch" id="waNotif"
                                        name="whatsapp" value="1" {{ Auth::user()->whatsapp ? 'checked' : '' }}>
                                    <label class="form-check-label" for="waNotif">Ingatkan dengan Whatsapp</label>
                                </div> --}}
                            </div>
                            <div class="w-100 d-flex justify-content-end">
                                <a class="text-decoration-none btn btn-outline-success px-4 py-2 btn-sm fw-semibold"
                                    href="#" onclick="checkInput()">Simpan</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/sweetalert11.7.5.min.js') }}"></script>
    <script>
        // document.addEventListener("DOMContentLoaded", () => {
        let current_email = "{{ Auth::user()->email }}";

        function checkInput() {
            if (current_email != document.getElementById('inputEmail').value) {
                Swal.fire({
                    title: 'Apa anda yakin?',
                    text: "Sistem mendeteksi bahwa anda ingin mengganti email anda, anda perlu melakukan verifikasi email ulang sebelum dapat mengakses kembali aplikasi ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, ganti itu!',
                    cancelButtonText: 'Tidak, jangan ganti!',
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Swal.fire(
                        //     'Berhasil!',
                        //     'Email anda berhasi dirubah!',
                        //     'success'
                        // );
                        document.getElementById('editProfileForm').submit()
                    }
                });
            } else {
                document.getElementById('editProfileForm').submit()
            }
        }

        function removePhoto(el) {
            Swal.fire({
                title: 'Apa anda yakin?',
                text: "Ingin menghapus foto profil anda? anda akan kembali menggunakan foto profil default setelahnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus itu!',
                cancelButtonText: 'Tidak, jangan dihapus!',
            }).then((result) => {
                if (result.isConfirmed) {
                    document.getElementById('deleteForm').setAttribute('action', el.getAttribute('data-url'));
                    document.getElementById('deleteForm').submit();
                }
            });
        }
        // });
    </script>
@endpush
