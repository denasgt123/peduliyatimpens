@extends('layouts.app')

@push('style')
    <style>
        body {
            background-color: #F6F7F7;
            min-height: 100%;
            min-width: 100%;
        }

        /* .navbar {
                                                            background-color: white;
                                                            min-height: 5rem;
                                                        } */

        .logo {
            max-height: 3rem;
        }

        .content {
            min-height: 10rem;
        }

        .donate-program-img {
            height: 30rem;
            object-fit: cover;
        }

        .bg-donation {
            background-color: #E8F6ED;
            color: #007139;
        }

        .img-donatur {
            width: 3.5rem;
            height: 3.5rem;
            object-fit: cover;
            object-position: center;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')

    {{-- Start Content Section --}}
    <div class="container h-100 w-100">
        {{-- Start Content Section --}}
        <section class="w-100 overflow-hidden">
            <div class="card border-0 d-flex flex-column shadow-sm overflow-hidden mt-3 mb-4">
                <img class="donate-program-img" src="{{ asset("img/asset/donateprogram/$program->photo") }}" alt="Program">
                <div class="card-body px-4 py-3">
                    <h3 class="fw-semibold mb-4 mt-2">{{ $program->name }}</h3>
                    <h6 class="fw-semibold">
                        <span class="pe-4"><i class="fa-solid fa-users pe-1 text-success"></i>
                            {{ $program->donation_count }}
                            Donatur</span>
                        <span><i class="fa-solid fa-calendar-days pe-1 text-success"></i>
                            {{ Carbon\Carbon::parse($program->end_at)->diff(Carbon\Carbon::now())->days }} hari lagi</span>
                    </h6>
                    <div class="progress my-4" role="progressbar" aria-label="Basic example"
                        aria-valuenow="{{ $program->progress }}" aria-valuemin="0" aria-valuemax="100" style="height: 20px">
                        <div class="progress-bar bg-success" style="width: {{ $program->progress }}%">
                            {{ $program->progress }}%</div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center mt-3">
                        <div class="fw-semibold">
                            <span class="bg-donation rounded-pill px-4 py-1 fw-bold me-2">
                                Rp.
                                {{ number_format($program->current_donate_amount, 0, ',', '.') }}
                            </span>
                            <small>Terdanai</small>
                        </div>
                        @if ($program->progress != 100)
                            <div class="fw-semibold">
                                <small>Kekurangan</small>
                                <span class="bg-donation rounded-pill px-4 py-1 fw-bold ms-2">
                                    Rp.
                                    {{ number_format($program->donate_goal - $program->current_donate_amount, 0, ',', '.') }}
                                </span>
                            </div>
                        @endif
                    </div>
                    <h5 class="mt-5 pt-2 fw-semibold">Deskripsi Program</h5>
                    <p>{{ $program->description }}</p>
                </div>
            </div>
            <div class="card mb-4 p-5 border-bottom-0 border-start-0 border-end-0 border-4 border-success">
                {{-- @dd($histories->count() > 0) --}}
                @if ($histories->count() > 0)
                    <h3 class="fw-semibold">Para Donatur ({{ $program->donation_count }})</h3>
                    <small class="fw-semibold mb-4">Nama-nama orang-orang baik yang berdonasi</small>
                    @foreach ($histories as $item)
                        {{-- @dd($item) --}}
                        @if ($item->is_anonim == 1)
                            <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                                <img class="img-donatur rounded-circle overflow-hidden me-4"
                                    src="{{ asset('img/user/user.png') }}" alt="Img"
                                    style="object-fit: cover; object-position: center;">
                                <div>
                                    <h5 class="mb-0 fw-semibold">Anonim <small
                                            class="ms-2 text-secondary fs-6">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                    </h5>
                                    <small class="fw-semibold">Donasi Rp.
                                        {{ number_format($item->amount_donation, 0, ',', '.') }},-</small>
                                </div>
                            </div>
                        @else
                            <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                                <img class="img-donatur rounded-circle overflow-hidden me-4"
                                    src="{{ asset('img/user/' . $item->donatur->photo) }}" alt="Img"
                                    style="object-fit: cover; object-position: center;">
                                <div>
                                    <h5 class="mb-0 fw-semibold">{{ $item->donatur->name }} <small
                                            class="ms-2 text-secondary fs-6">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                    </h5>
                                    <small class="fw-semibold">Donasi RP.
                                        {{ number_format($item->amount_donation, 0, ',', '.') }},-</small>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                        <img class="img-donatur rounded-circle overflow-hidden me-4"
                            src="{{ asset('img/user/denas-small_1684734725.png') }}" alt="Img"
                            style="object-fit: cover; object-position: center;">
                        <div>
                            <h5 class="mb-0 fw-semibold">Denassyah Nurrohman <small class="ms-2 text-secondary fs-6">13 hari
                                    yang
                                    lalu</small></h5>
                            <small class="fw-semibold">Donasi RP. 10.000</small>
                        </div>
                    </div>
                    <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                        <img class="img-donatur rounded-circle overflow-hidden me-4" src="{{ asset('img/user/user.png') }}"
                            alt="Img" style="object-fit: cover; object-position: center;">
                        <div>
                            <h5 class="mb-0 fw-semibold">Anonymous <small class="ms-2 text-secondary fs-6">13 hari yang
                                    lalu</small></h5>
                            <small class="fw-semibold">Donasi RP. 10.000</small>
                        </div>
                    </div>
                @else
                    <div class="py-4 d-flex align-items-center ps-1">
                        <h4 class="fw-semibold">Belum ada riwayat donasi untuk program ini</h4>
                    </div>
                @endif
                {{-- Carbon\Carbon::parse($item->end_at)->diffForHumans() --}}
                {{-- <h6 class="fw-semibold mt-3">Belum ada Donatur saat ini</h6> --}}
            </div>
        </section>
        {{-- End Content Section --}}
    </div>
    <div class="sticky-bottom d-flex align-items-center justify-content-center card">
        <a href="{{ route('create-donation', $program->slug) }}"
            class="btn btn-success px-5 py-2 rounded-pill my-3 fw-semibold fs-5 text-decoration-none text-white"
            style="width: 40rem;">
            Donasi Sekarang!
        </a>
    </div>
    {{-- End Content Section --}}
@endsection
