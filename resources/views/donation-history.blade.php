@extends('layouts.app')

@push('style')
    <style>
        .header {
            min-height: 20rem;
        }

        .program-item {
            height: 11.5rem;
        }

        .img-program {
            height: 100%;
            width: 20%;
            object-fit: cover;
            object-position: center;
        }

        .desc-program {
            height: 100%;
            width: 80%;
        }

        .bg-donation {
            background-color: #E8F6ED;
            color: #007139;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables1.13.4.min.css') }}">
@endpush

@section('content')
    @include('layouts.navbar')
    {{-- Start Content Section --}}
    <div class="container vw-100">
        {{-- Start Header Section --}}
        <section class="w-100 position-relative overflow-hidden header">
            <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle "
                src="{{ asset('img/asset/home/bg atas.png') }}" alt="Background Img" style="object-fit: cover;">
            <div
                class="position-absolute top-50 start-50 translate-middle px-5 row justify-content-center align-items-center w-100">
                <div class="col-6">
                    <h3 class="fw-semibold">
                        <span class="border-success border-3 border-bottom">
                            Riwayat Transaksi Donasi
                        </span>
                    </h3>
                    <h6 class="mb-0 mt-4 fw-semibold">
                        Di sini, Anda dapat melihat dan melacak semua transaksi donasi yang telah Anda lakukan melalui
                        Aplikasi Yayasan Peduli Yatim PENS-ITS. Halaman ini menyajikan riwayat lengkap transaksi donasi Anda
                        sebagai bentuk
                        transparansi dan pertanggungjawaban.
                    </h6>
                </div>
                <div class="col-2 offset-1 px-0">
                    <img class="w-100 h-100" src="{{ asset('img/logo.png') }}" alt="logo" style="object-fit: contain;">
                </div>
                <div class="col-12">

                </div>
            </div>
        </section>
        {{-- End Header Section --}}

        {{-- Start List Donation History Section --}}
        <div class="card border-0 mt-4">
            <div class="card-body p-4">
                <h5 class="fw-semibold mb-3">
                    Filter Riwayat Donasi
                </h5>
                <div class="d-flex flex-wrap mb-5">
                    @foreach ($permanent_programs as $key => $item)
                        <a href="{{ route('donation-history', $item->slug) }}"
                            class="btn {{ $active_filter == $item->id ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2 text-decoration-none"
                            type="button">{{ $item->shortname }}</a>
                        {{-- <button
                            class="btn {{ $active_filter == 2 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2"
                            type="button">Zakat</button>
                        <button
                            class="btn {{ $active_filter == 3 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2"
                            type="button">Waqaf Tanah</button>
                        <button
                            class="btn {{ $active_filter == 4 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2"
                            type="button">Infaq Swakelola</button>
                        <button
                            class="btn {{ $active_filter == 5 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2"
                            type="button">Modal</button>
                        <button
                            class="btn {{ $active_filter == 6 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2"
                            type="button">Program Lainnya</button> --}}
                    @endforeach
                    <a href="{{ route('donation-history', 'program-lainnya') }}"
                        class="btn {{ $active_filter == 6 ? 'btn-success' : 'btn-outline-success' }} btn-sm px-4 rounded-pill me-2 text-decoration-none"
                        type="button">Program Lainnya</a>
                </div>
                <h4 class="fw-semibold mb-4">
                    List Riwayat Donasi {{ $program_name }}
                </h4>
                {{-- <div class="d-flex align-items-center mb-4">
                    <p class="mb-0">
                        Dibawah ini merupakan List Riwayat Donasi {{ $program_name }}, jika ingin melihat riwayat donasi
                        lainnya, pilih
                    </p>
                    <div class="dropdown ms-2">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Riwayat Donasi
                        </button>
                        <ul class="dropdown-menu">
                            @foreach ($permanent_programs as $program)
                                <li>
                                    <a class="dropdown-item" href="{{ route('donation-history', $program->slug) }}">
                                        {{ $program->name }}
                                    </a>
                                </li>
                            @endforeach
                            <li>
                                <a class="dropdown-item" href="{{ route('donation-history', 'program-lainnya') }}">
                                    Program Lainnya
                                </a>
                            </li>
                        </ul>
                    </div>
                </div> --}}
                @if ($program_name != 'Program Lainnya')
                    <table id="myTable" class="display table table-striped py-4">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nominal Donasi</th>
                                <th>Tanggal Donasi</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($histories as $item)
                                <tr onclick="redirect('{{ route('detail-donation', $item->transaction_id) }}')"
                                    style="cursor: pointer;">
                                    <td lass="fw-bold">{{ $item->transaction_id }}</td>
                                    <td>{{ 'Rp. ' . number_format($item->amount_donation, 2) }}</td>
                                    <td>{{ $item->created_at->format('d F Y') }}</td>
                                    <td class="text-center">
                                        <span
                                            class="badge text-bg-{{ $status[$item->status]['color'] }} px-3 rounded-pill">{{ $status[$item->status]['name'] }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <table id="myTable" class="display table table-striped table-hover py-4">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Progam Donasi</th>
                                <th>Nominal Donasi</th>
                                <th>Tanggal Donasi</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($histories as $item)
                                <tr onclick="redirect('{{ route('detail-donation', $item->transaction_id) }}')"
                                    style="cursor: pointer;">
                                    <td class="fw-bold">{{ $item->transaction_id }}</td>
                                    <td>{{ $item->program->name }}</td>
                                    <td>{{ 'Rp. ' . number_format($item->amount_donation, 2) }}</td>
                                    <td>{{ $item->created_at->format('d F Y') }}</td>
                                    <td class="text-center">
                                        <span
                                            class="badge text-bg-{{ $status[$item->status]['color'] }} px-3 rounded-pill">{{ $status[$item->status]['name'] }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        {{-- End List Donation History Section --}}
    </div>
    {{-- End Content Section --}}
@endsection

@push('script')
    <script src="{{ asset('js/jquery-3.7.0.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables1.13.4.min.js') }}"></script>
    <script>
        function redirect(url) {
            location.replace(url);
        }

        $(document).ready(function() {
            $('#myTable').DataTable({
                "language": {
                    "emptyTable": "Maaf, belum ada riwayat donasi dari Anda untuk Program Donasi ini."
                }
            });
        });
    </script>
@endpush
