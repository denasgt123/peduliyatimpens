@extends('layouts.app')

@push('style')
    <link rel="stylesheet" href="{{ asset('css/swiper9.3.2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/home.css') }}" />
@endpush

@section('content')
    @include('layouts.navbar')
    {{-- Start Content Section --}}
    <div class="container vw-100 min-vh-100">
        {{-- Start Header Section --}}
        <section class="w-100 position-relative overflow-hidden header">
            <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle "
                src="{{ asset('img/asset/home/bg atas.png') }}" alt="Background Img" style="object-fit: cover;">
            <div
                class="position-absolute top-50 start-50 translate-middle px-5 d-flex justify-content-center align-items-center flex-wrap w-100">
                <div class="col-6">
                    <h4 class="fw-semibold">
                        Mari kita tunjukkan kepedulian kita kepada sesama dengan berdonasi pada yayasan <br><span
                            class="fw-bold fs-3 border-success border-bottom border-3">Peduli
                            Yatim PENS-ITS</span>
                    </h4>
                </div>
                <div class="col-2 offset-1">
                    <img class="w-100 h-100" src="{{ asset('img/logo.png') }}" alt="logo" style="object-fit: contain;">
                </div>
            </div>
        </section>
        {{-- End Header Section --}}

        {{-- Start Permanent Donate Program Section --}}
        <div class="card border-0 mt-4">
            <div class="row g-0 px-2 py-3">
                {{-- <a href="javascript:void(0)"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Calendar.svg') }}" alt="">
                    <b class="text-program-permanent">Donasi Rutin</b>
                </a> --}}
                <a href="{{ route('fixed-program', $donate_programs[0]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Infaq Yatim.svg') }}" alt="">
                    <b class="text-program-permanent">Infaq <br>Yatim Dhuafa</b>
                </a>
                <a href="{{ route('fixed-program', $donate_programs[1]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Zakat.svg') }}" alt="">
                    <b class="text-program-permanent">Zakat</b>
                </a>
                <a href="{{ route('fixed-program', $donate_programs[2]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Waqaf.svg') }}" alt="">
                    <b class="text-program-permanent">Waqaf Tanah</b>
                </a>
                <a href="{{ route('fixed-program', $donate_programs[3]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Swakelola.svg') }}" alt="">
                    <b class="text-program-permanent">Infaq Swakelola</b>
                </a>
                <a href="{{ route('fixed-program', $donate_programs[4]->slug) }}"
                    class="col d-flex justify-content-center align-items-center text-decoration-none text-dark">
                    <img class="pe-2" src="{{ asset('img/asset/home/Modal.svg') }}" alt="">
                    <b class="text-program-permanent">Modal</b>
                </a>
            </div>
        </div>
        {{-- End Permanent Donate Program Section --}}

        {{-- Start Temporarily Donate Program Section --}}
        <div class="card mt-4 w-100">
            <div class="temp-prog-header d-flex justify-content-between align-items-center px-5">
                <h2>Donasi Pilihan</h2>
                <div class="d-flex">
                    <div class="circle-arrow d-flex justify-content-center align-items-center me-2 btn swiper-prev">
                        <i class="fa-solid fa fa-chevron-left"></i>
                    </div>
                    <div class="circle-arrow d-flex justify-content-center align-items-center btn swiper-next"><i
                            class="fa-solid fa fa-chevron-right"></i></div>
                </div>
            </div>
            <div class="swiper mySwiper px-5 pb-5 w-100">
                <div class="swiper-wrapper pb-2">
                    @forelse ($temp_donate_programs as $item)
                        <div class="swiper-slide">
                            <div
                                class="card border border-opacity-25 d-flex flex-column shadow-sm overflow-hidden mb-3 temp-item">
                                <img src="{{ asset("img/asset/donateprogram/$item->photo") }}" alt=""
                                    class="card-img-top temp-img">
                                <div class="card-body">
                                    <div class="d-flex flex-column flex-wrap h-100">
                                        <div class="w-100 temp-desc mb-2 flex-grow-1">
                                            <h6 class="fw-semibold fs-6">{{ $item->name }}</h6>
                                            <small>{{ $item->description }}</small>
                                        </div>
                                        <div class="w-100">
                                            <small class="text-secondary-emphasis fw-semibold"
                                                style="font-size: 0.7rem;">Dana
                                                terkumpul</small>
                                            <div class="progress" role="progressbar" aria-label="Basic example"
                                                aria-valuenow="{{ $item->progress }}" aria-valuemin="0" aria-valuemax="100"
                                                style="height: 8px;">
                                                <div class="progress-bar bg-success" style="width: {{ $item->progress }}%">
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center mt-3">
                                                <span class="badge bg-donation rounded-pill px-4 py-1 fw-bold">
                                                    Rp.
                                                    {{ number_format($item->current_donate_amount, 0, ',', '.') }},-
                                                </span>
                                                {{-- <span class="badge text-bg-secondary">
                                                    {{ Carbon\Carbon::parse($item->end_at)->diffForHumans() }}
                                                </span> --}}
                                                <span class="badge text-dark fw-semibold">
                                                    {{ Carbon\Carbon::parse($item->end_at)->diff(Carbon\Carbon::now())->days }}
                                                    hari lagi
                                                </span>
                                            </div>

                                            <a href="{{ route('detail-program', $item->slug) }}"
                                                class="w-100 btn btn-success mt-3 text-decoration-none text-white fw-semibold"
                                                style="border-radius: 30px">Donasi
                                                Sekarang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="d-flex flex-column justify-content-between">
                                <img src="{{ asset('img/asset/home/No Data Illustration.svg') }}" alt="img"
                                    style="height: 15rem; object-postion: center; object-fit: contain;">
                                <h5 class="mb-5 fw-semibold mt-5">Maaf, sementara belum ada program donasi saat ini</h5>
                            </div>
                        </div>
                    @endforelse
                </div>
                <div class="swiper-pagination mb-3"></div>
            </div>
        </div>
        {{-- End Temporarily Donate Program Section --}}

        {{-- Start Promote App Section --}}
        <div class="my-4 position-relative">
            <img class="w-100" src="{{ asset('img/asset/home/banner donwload minus button only.svg') }}"
                alt="">
            <a class="position-absolute" style="top:65%;left: 100px" href="https://play.google.com/store/"
                target="blank"><img src="{{ asset('img/asset/home/button gplay download.svg') }}" alt=""></a>
        </div>
        {{-- End Promote App Section --}}

        {{-- <div class="w-100 bg-primary mt-5" style="height: 100px;">

        </div> --}}
    </div>
    {{-- End Content Section --}}
@endsection

@push('script')
    <script src="{{ asset('js/swiper9.3.2.min.js') }}"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 3,
            spaceBetween: 30,
            freeMode: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-next',
                prevEl: '.swiper-prev',
            },
        });
    </script>
@endpush
