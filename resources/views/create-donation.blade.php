@extends('layouts.app')

@push('style')
    <style>
        .img-payment {
            width: auto;
            height: 3rem;
            object-fit: contain;
            object-position: center;
        }

        .bg-donation {
            background-color: #E8F6ED;
            color: #007139;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')
    <div class="container">
        <form action="{{ route('store-donation', $program->slug) }}" method="POST">
            @csrf
            <div class="card mb-4 p-5">
                <h4 class="fw-semibold mb-5">Ingin berdonasi ke program "{{ $program->name }}"?</h4>
                <input type="hidden" name="gross_amount" id="gross_amount" value="">
                <input type="hidden" name="payment_method" id="payment_method" value="">
                <input type="hidden" name="net_amount" id="net_amount" value="">
                <label for="inputNominalDonasi" class="form-label fw-semibold">Masukan Nominal Donasi</label>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Rp.</span>
                    <input class="form-control form-control-lg" type="text" placeholder="Nominal Donasi Anda"
                        id="inputNominalDonasi">
                </div>
                @auth
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" role="switch" id="anonimMode" name="hide_me"
                            value="1" {{ Auth::user()->hide_me ? 'checked' : '' }}>
                        <label class="form-check-label" for="anonimMode">Sembunyikan nama saya</label>
                    </div>
                @endauth
                <div class="row mt-3 g-3">
                    <div class="col-4">
                        <button class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="5,000">
                            Rp. 5.000
                        </button>
                    </div>
                    <div class="col-4">
                        <button type="button"
                            class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="10,000">
                            Rp. 10.000
                        </button>
                    </div>
                    <div class="col-4">
                        <button type="button"
                            class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="20,000">
                            Rp. 20.000
                        </button>
                    </div>
                    <div class="col-4">
                        <button type="button"
                            class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="50,000">
                            Rp. 50.000
                        </button>
                    </div>
                    <div class="col-4">
                        <button type="button"
                            class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="100,000">
                            Rp. 100.000
                        </button>
                    </div>
                    <div class="col-4">
                        <button type="button"
                            class="btn btn-price btn-outline-success w-100 rounded-pill btn-lg fw-semibold"
                            data-value="500,000">
                            Rp. 500.000
                        </button>
                    </div>
                </div>
            </div>
            <div class="card mb-4 px-4 py-5 border-bottom-0 border-start-0 border-end-0 border-4 border-success">
                <h4 class="fw-semibold mb-5">Pilih Metode Pembayaran</h4>
                @foreach ($payment_categories as $key => $category)
                    @if ($category->methods->count() == 0)
                        @continue
                    @endif
                    <h6 class="fw-semibold mb-2">{{ $category->name }}</h6>
                    <div class="row g-3 mb-5">
                        @forelse ($category->methods as $method)
                            @if ($method->is_active == 0)
                                @continue
                            @endif
                            <div class="col-6">
                                <button type="button"
                                    class="bg-white btn-payment-method text-secondary py-3 w-100 border border-dark border-opacity-50 btn-sm d-flex align-items-center justify-content-between py-3 px-4 rounded"
                                    style="cursor: pointer;">
                                    <img src="{{ asset("img/pembayaran/".$method->logo) }}" style="height: 2rem;" alt="{{ $method->name }}"
                                        class="img-payment">
                                    <h6 class="fw-semibold fst-italic mb-0 price-list" data-fee="{{ $method->fee }}"
                                        data-type="{{ $method->fee_type }}">Otomatis</h6>
                                </button>
                            </div>
                        @empty
                            <div class="col-12">
                                <h5 class="text-center mb-0 mt-3">
                                    Metode Pembayaran Belum Tersedia.
                                </h5>
                            </div>
                        @endforelse
                    </div>
                @endforeach
            </div>
            <div class="card mb-4 p-4">
                <h6 class="fw-semibold">Rincian Donasi</h6>
                <div class="d-flex justify-content-between">
                    <small>Nominal Donasi :</small>
                    <small class="fst-italic text-secondary donate-price">Otomatis</small>
                </div>
                <div class="d-flex justify-content-between">
                    <small>Biaya Pembayaran :</small>
                    <small class="fst-italic text-secondary donate-fee">Otomatis</small>
                </div>
                <div class="d-flex justify-content-between mt-1 pt-2 border-top border-dark">
                    <h6 class="fw-semibold mb-0">Total Pembayaran :</h6>
                    {{-- <h6 class="text-success fw-semibold">Rp. 104.000,-</h6> --}}
                    <h6 class="text-success fw-semibold donate-total">Otomatis</h6>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-4">
                <button type="submit"
                    class="btn btn-success btn-lg rounded-pill my-3 fw-semibold fs-5 text-decoration-none text-white"
                    style="width: 40rem;">
                    Berikan Donasi!
                </button>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js"
        integrity="sha512-KaIyHb30iXTXfGyI9cyKFUIRSSuekJt6/vqXtyQKhQP6ozZEGY8nOtRS6fExqE4+RbYHus2yGyYg1BrqxzV6YA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function checkPrice(el) {
            let donate_price_cleaved = $(el).val();
            let donate_price = parseInt(donate_price_cleaved.split(",").join(""));

            $('.price-list').each(function(index, element) {
                let fee = $(element).data('fee') == 0.7 ? 0.7 : parseInt($(element).data('fee'));
                let type = $(element).data('type');
                let total = 0;
                let addon = 0;

                if (donate_price_cleaved != "") {
                    if (type == "flat_ppn") {
                        addon = fee + (fee * 11 / 100);
                        // total = Math.ceil(donate_price + addon);
                    } else if (type == "flat") {
                        addon = fee;
                        // total = Math.ceil(donate_price + addon);
                    } else {
                        addon = ((donate_price * fee) / 100) + ((((donate_price * fee) / 100) * 11) / 100);
                        // total = Math.ceil(donate_price + addon);
                    }
                    total = Math.ceil(donate_price + addon);
                }
                $(element).parent().data('feeprice', addon)
                $(element).html("Rp. " + numberWithCommas(total));
            });

            setPrice($('.btn-payment-method > .bg-donation'));
        }

        function setPrice(e) {
            let payment_method = $(e.currentTarget).children(":first").attr("alt");
            // console.log($(e.currentTarget).children(":first").attr("alt"));
            let price = $(e.currentTarget).find('.price-list').html().substring(4);
            let gross_amount = parseInt(price.split(",").join(""));
            let net_amount = parseInt($("#inputNominalDonasi").val().split(",").join(""));
            $("#payment_method").val(payment_method);
            $("#gross_amount").val(gross_amount);
            $("#net_amount").val(net_amount);

            $('.donate-price').html("Rp. " + $('#inputNominalDonasi').val());
            $('.donate-fee').html("Rp. " + numberWithCommas($(e.currentTarget).data('feeprice')));
            $('.donate-total').html($(e.currentTarget).find('.price-list').html());
        }

        $(document).ready(function() {
            $('.btn-price').click(function(e) {
                e.preventDefault();
                $('#inputNominalDonasi').val($(this).data('value'));
                checkPrice($('#inputNominalDonasi'));
            });

            $('#inputNominalDonasi').change(function(e) {
                checkPrice($(this));
            });

            $('.btn-payment-method').click(function(e) {
                e.preventDefault();
                // console.log(e.currentTarget);
                if ($('#inputNominalDonasi').val() != "") {
                    $('.bg-donation').addClass("text-secondary");
                    $('.bg-donation').addClass("bg-white");
                    $('.bg-donation').removeClass("shadow");
                    $('.bg-donation').removeClass("bg-donation");
                    $(e.currentTarget).addClass("bg-donation");
                    $(e.currentTarget).addClass("shadow");
                    $(e.currentTarget).removeClass("bg-white");
                    $(e.currentTarget).removeClass("text-secondary");
                    setPrice(e);
                } else {
                    alert(
                        'Harap masukan nominal donasi terlebih dahulu sebelum memilih metode pembayaran!'
                    );
                }
            });


            var cleave = new Cleave('#inputNominalDonasi', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });
        });
    </script>
@endpush
