@extends('layouts.app')

@push('style')
    <style>
        .header {
            min-height: 20rem;
        }

        .program-item {
            height: 11.5rem;
        }

        .img-program {
            height: 100%;
            width: 20%;
            object-fit: cover;
            object-position: center;
        }

        .desc-program {
            height: 100%;
            width: 80%;
        }

        .bg-donation {
            background-color: #E8F6ED;
            color: #007139;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')
    {{-- Start Content Section --}}
    <div class="container vw-100">
        {{-- Start Header Section --}}
        <section class="w-100 position-relative overflow-hidden header mb-4">
            <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle "
                src="{{ asset('img/asset/home/bg atas.png') }}" alt="Background Img" style="object-fit: cover;">
            <div
                class="position-absolute top-50 start-50 translate-middle px-5 row justify-content-center align-items-center w-100">
                <div class="col-6">
                    <h3 class="fw-semibold">
                        @if ($donation->status == 'gagal')
                            <span class="border-success border-3 border-bottom">
                                Mohon Maaf
                            </span>
                        @else
                            <span class="border-success border-3 border-bottom">
                                Terima Kasih
                            </span>
                        @endif
                    </h3>
                    @if (!$is_timeout && $donation->status == 'menunggu')
                        <h6 class="mb-0 mt-4 fw-semibold">
                            Silahkan selesaikan proses pembayaran Anda sebelum tenggat waktunya, Donasi yang anda berikan
                            sangat berarti bagi kami.
                        </h6>
                    @else
                        @if ($donation->status == 'sukses')
                            <h6 class="mb-0 mt-4 fw-semibold">
                                Terima kasih telah memberikan donasinya pada kami, Donasi yang anda berikan sangat berarti
                                bagi kami.
                            </h6>
                        @else
                            <h6 class="mb-0 mt-4 fw-semibold">
                                Terjadi kesalahan saat pada proses pembayaran sehingga donasi Anda tidak berhasil diproses
                                dengan sukses. Meskipun demikian, Kami mengucapkan terima kasih atas dukungan Anda yang
                                berniat ingin membantu kami membuat kehidupan menjadi lebih baik 🙏
                            </h6>
                        @endif
                    @endif
                </div>
                <div class="col-2 offset-1 px-0">
                    <img class="w-100 h-100" src="{{ asset('img/logo.png') }}" alt="logo" style="object-fit: contain;">
                </div>
            </div>
        </section>
        {{-- End Header Section --}}

        @if (!$is_timeout && $donation->status == 'menunggu')
            <div class="alert alert-success fw-semibold" role="alert">
                Harap selesaikan proses pembayaran sebelum <small
                    class="fw-bold h6 ms-3 py-1 px-4 bg-success text-white rounded-pill float-end"
                    id="timer">00:00:00</small>
            </div>
        @endif

        {{-- <div class="alert alert-danger" role="alert">
            Maaf, Transaksi yang anda lakukan telah kadaluarsa
        </div> --}}

        {{-- Start Donation Detail-1 Section --}}
        <div class="card mb-4 p-5 border-bottom-0 border-start-0 border-end-0 border-4 border-success">
            <div
                class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                <h5 class="mb-0">Nomor Donasi</h5>
                <div class="d-flex flex-wrap align-items-center">
                    <button class="btn btn-success btn-sm px-3 me-3" onclick="copyToClipboard(this)">Salin</button>
                    <h5 class="fw-bold mb-0">#{{ $donation->transaction_id }}</h5>
                </div>
            </div>
            <div
                class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                <h5 class="mb-0">Nama Program Donasi</h5>
                <h5 class="mb-0">{{ $donation->program->name }}</h5>
            </div>
            <div
                class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                <h5 class="mb-0">Nominal Donasi</h5>
                <h5 class="mb-0">Rp. {{ number_format($donation->amount_donation, 0, ',', '.') }}</h5>
            </div>
            <div
                class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                <h5 class="mb-0">Status Donasi</h5>
                <div
                    class="px-4 py-1 border {{ $donation->status == 'menunggu' ? 'border-2 border-secondary text-secondary' : ($donation->status == 'sukses' ? 'border-success text-success' : 'border-danger text-danger') }} rounded-pill ">
                    <small class="fw-semibold">{{ ucfirst($donation->status) }}</small>
                </div>
                {{-- @dd($donation->status) --}}
                {{-- @if ($donation->status == 'menunggu')
                    <div
                        class="px-4 py-1 border {{ $donation->status == 'menunggu' ? 'border-2 border-secondary text-secondary' : ($donation->status == 'settlement' ? 'border-success text-success' : 'border-danger text-danger') }} rounded-pill ">
                        <small class="fw-semibold">Menunggu</small>
                    </div>
                @elseif ($donation->status == 'berhasil')
                    <div class="px-4 py-1 border border-success rounded-pill text-success">
                        <small class="fw-semibold">Berhasil</small>
                    </div>
                @else
                    <div class="px-4 border border-2 border-danger rounded-pill text-danger">
                        <small class="fw-semibold">Gagal</small>
                    </div>
                @endif --}}
            </div>
            <div
                class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                <h5 class="mb-0">Waktu Donasi</h5>
                <h5 class="mb-0">{{ Carbon\Carbon::parse($donation->created_at)->format('d-M-Y H:i:s') }}</h5>
            </div>
            {{-- Carbon\Carbon::parse($item->end_at)->diffForHumans() --}}
            {{-- <h6 class="fw-semibold mt-3">Belum ada Donatur saat ini</h6> --}}
            @if ($is_timeout && $donation->status != 'menunggu')
                <div
                    class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                    <h5 class="mb-0">Metode Pembayaran</h5>
                    <div class="d-flex flex-wrap align-items-center">
                        <img src="{{ asset('img/pembayaran/' . $donation->paymentMethod->logo) }}" alt="img"
                            style="height: 2rem;">
                        <h5 class="ms-3 mb-0 fw-bold">{{ strtoupper($donation->name) }}</h5>
                    </div>
                </div>
            @endif
        </div>
        {{-- End Donation Detail-1 Section --}}

        @if (!$is_timeout && $donation->status == 'menunggu')
            <div class="alert alert-warning fw-semibold" role="alert">
                Harap melakukan pembayaran sesuai dengan nominal yang tertara. Tidak lebih dan tidak kurang!
            </div>

            {{-- Start Donation Detail-2 Section --}}
            <div class="card mb-4 p-5 border-bottom-0 border-start-0 border-end-0 border-4 border-success">
                <div
                    class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                    <h5 class="mb-0">Total Pembayaran</h5>
                    <div class="d-flex flex-wrap align-items-center">
                        <button class="btn btn-success btn-sm px-3 me-3" onclick="copyAmountDonation(this)">Salin</button>
                        <h5 class="mb-0 fw-bold">Rp. {{ number_format((int) $midtrans->gross_amount, 0, ',', '.') }}</h5>
                    </div>
                </div>
                <div
                    class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                    <h5 class="mb-0">Metode Pembayaran</h5>
                    <div class="d-flex flex-wrap align-items-center">
                        <img src="{{ asset('img/pembayaran/' . $donation->paymentMethod->logo) }}" alt="img"
                            style="height: 2rem;">
                        <h5 class="ms-3 mb-0 fw-bold">{{ strtoupper($donation->name) }}</h5>
                    </div>
                </div>
                @foreach ($donation->payment_target as $key => $item)
                    <div
                        class="py-3 px-2 d-flex flex-wrap justify-content-between align-items-center w-100 border-bottom border-opacity-50">
                        <h5 class="mb-0">{{ $key }}</h5>
                        @if ($key == 'QRIS')
                            <div class="d-flex flex-wrap align-items-center">
                                <img src="{{ $item }}" alt="qris" style="width: 30rem; height: auto;">
                            </div>
                        @else
                            <div class="d-flex flex-wrap align-items-center">
                                <button class="btn btn-success btn-sm px-3 me-3"
                                    onclick="copyToClipboard(this)">Salin</button>
                                <h5 class="fw-bold mb-0">#{{ $item }}</h5>
                            </div>
                        @endif
                    </div>
                @endforeach
                {{-- Carbon\Carbon::parse($item->end_at)->diffForHumans() --}}
                {{-- <h6 class="fw-semibold mt-3">Belum ada Donatur saat ini</h6> --}}
            </div>
            {{-- End Donation Detail-2 Section --}}
        @endif
    </div>
    {{-- End Content Section --}}
@endsection

@push('script')
    <script>
        function copyToClipboard(e) {
            let text = $(e).next().html().substring(1);
            navigator.clipboard.writeText(text);
            alert("Berhasil Menyalin : " + text);
        }

        function copyAmountDonation(e) {
            let text = $(e).next().html().substring(4);
            navigator.clipboard.writeText(text);
            alert("Berhasil Menyalin : " + text);
        }

        $(document).ready(function() {
            let timer2 = '{{ $timelimit }}';
            // let timer2 = '00:00:10';
            const myinterval = setInterval(function() {
                let timer = timer2.split(':');
                // by parsing integer, I avoid all extra string processing
                let hours = parseInt(timer[0], 10);
                let minutes = parseInt(timer[1], 10);
                let seconds = parseInt(timer[2], 10);
                --seconds;
                if (hours == 0 && minutes == 0 && seconds < 0) {
                    location.reload();
                    clearInterval(myinterval);
                } else {
                    hours = (minutes < 0) ? --hours : hours;
                    hours = (hours < 10 && hours >= 0) ? '0' + hours : hours;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    minutes = (minutes < 10 && minutes >= 0) ? '0' + minutes : minutes;
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10 && seconds >= 0) ? '0' + seconds : seconds;
                }

                // minutes = (minutes < 10) ?  minutes : minutes;
                $('#timer').html(hours + ':' + minutes + ':' + seconds);
                timer2 = hours + ':' + minutes + ':' + seconds;
            }, 1000);

            const check_status = setInterval(function() {
                $.ajax("{{ route('check-status', $donation->transaction_id) }}", {
                    success: function(status) { // success callback function
                        // console.log(status == 'sukses');
                        if (status == 'sukses') {
                            location.reload();
                        }
                    }
                });
            }, 5000);

            // setInterval(function() {
            //     let result = $.ajax({
            //             type: 'GET',
            //             url: "{{ env('MIDTRANS_SANDBOX_API_URL') . $donation->transaction_id . '/status' }}",
            //             contentType: 'application/json',
            //             headers: {
            //                 Accept: 'application/json',
            //                 Authorization: 'Basic ' + btoa("env('MIDTRANS_SERVER_KEY')" + ":"),
            //             }
            //         })
            //         .done(function(data) {
            //             console.log(data);
            //             alert(data);
            //         });
            //     // $.ajax({
            //     //         type: 'GET',
            //     //         url: "{{ env('MIDTRANS_SANDBOX_API_URL') . $donation->transaction_id . '/status' }}",
            //     // 		contentType: 'application/json',
            //     //         headers: {
            //     //             Accept: 'application/json',
            //     //             Authorization: 'Basic ' + btoa("env('MIDTRANS_SERVER_KEY')" + ":"),
            //     //         }
            //     //     })
            //     //     .done(function(data) {
            //     //         console.log(data);
            //     //         alert(data);
            //     //     });
            //     // $.get("{{ env('MIDTRANS_SANDBOX_API_URL') . $donation->transaction_id . '/status' }}",
            //     //     function(data, status) {
            //     //         alert("Status: " + status);
            //     //         console.log(data);
            //     //     });
            //     console.log(result);
            // }, 5000);
        });
    </script>
@endpush
