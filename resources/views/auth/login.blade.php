@extends('layouts.app')

@push('style')
    <style>
        .bg-image-vertical {
            position: relative;
            overflow: hidden;
            background-repeat: no-repeat;
            background-position: right center;
            background-size: auto 100%;
        }

        @media (min-width: 1025px) {
            .h-custom-2 {
                height: 100%;
            }
        }
    </style>
@endpush

@section('content')
    <section class="vh-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xxl-4 col-lg-5 col-12 text-black py-5">
                    <div class="px-5 mt-3 ms-xl-4">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo" height="95" width="95">
                        </a>
                        <div class="my-4">
                            <p class="fs-3">Selamat Datang di <br><span
                                    class="fw-semibold">{{ config('app.name', 'Peduli Yatim PENS-ITS') }}</span></p>
                            <p class="mb-0 fs-6">Sebelum dapat mengakses aplikasi kami, harap login terlebih dahulu atau
                                mendaftar jika belum memiliki akun! 😊🙏</p>
                        </div>
                    </div>

                    <div class="d-flex align-items-center px-5 mt-5 ms-xl-4">
                        <form method="POST" action="{{ route('login') }}" class="w-100">
                            @csrf
                            <div class="mb-4">
                                <label for="email" class="form-label mb-3">Email address</label>
                                <input type="email" class="form-control py-2 @error('email') is-invalid @enderror"
                                    id="email" placeholder="name@example.com" autocomplete="email"
                                    value="{{ old('email') }}" name="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-4">
                                <label for="password" class="form-label mb-3">Password</label>
                                <a class="float-end" href="{{ route('password.request') }}"><small>Lupa
                                        password?</small></a>
                                <input type="password" id="password"
                                    class="form-control py-2 @error('password') is-invalid @enderror"
                                    aria-labelledby="passwordHelpBlock" name="password">

                                {{-- <div id="passwordHelpBlock" class="form-text">
                                    Your password must be 8-20 characters long, contain letters and numbers, and must not
                                    contain spaces, special characters, or emoji.
                                </div> --}}

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-check mb-4">
                                <input class="form-check-input" type="checkbox" value="1" id="rememberMe"
                                    name="remember">
                                <label class="form-check-label" for="rememberMe">
                                    <small>Ingat Akun Saya di Browser ini</small>
                                </label>
                            </div>

                            <button class="btn btn-primary w-100 rounded" type="submit">Masuk</button>

                            <div class="row my-3">
                                <div class="col-5 gx-0">
                                    <hr>
                                </div>
                                <div class="col-2 gx-0">
                                    <p class="text-muted text-center mb-0">Atau</p>
                                </div>
                                <div class="col-5 gx-0">
                                    <hr>
                                </div>
                            </div>

                            <a href="{{ route('google') }}" class="btn w-100 google-btn mb-3 text-decoration-none neumorph-light"
                                style="background-color: #dd4b39; color: white;" type="submit"><i
                                    class="fab fa-google"></i> Masuk dengan akun Google</a>

                            <p>Belum memiliki akun? <a href="{{ route('register') }}">Daftar sekarang!</a></p>

                        </form>
                    </div>

                    {{-- <div class="mt-auto">
                        <p class="mb-0 text-center px-3 fw-semibold">&copy; 2022 Peduli Yatim Pens, org. All right reserved.
                        </p>
                    </div> --}}

                </div>
                <div class="col-xxl-8 col-lg-7 col-12 px-0 d-none d-lg-block overflow-hidden">
                    <img src="{{ asset('img/asset/loginpage/kids.png') }}" alt="Login image" class="w-100 vh-100"
                        style="object-fit: cover; object-position: center;">
                </div>
            </div>
        </div>
    </section>
@endsection
