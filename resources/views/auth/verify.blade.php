@extends('layouts.app')

@push('style')
    <style>
        .title-text-first {
            font-family: 'Chewy' !important;
            color: #3f3d56;
        }

        .title-text-last {
            font-family: 'Chewy' !important;
            color: #006934;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-8">
                <div class="w-100 my-2 d-flex justify-content-center align-items-center">
                    <div class="rounded-circle neumorph-light p-3">
                        <img src="{{ asset('img/logo.png') }}" alt="Logo" style="height: 20rem; width: auto;">
                    </div>
                </div>
                <div class="w-100 my-3">
                    <h1 class="text-center mb-0t" style="font-size: 5rem;"><span class="title-text-first">Terima</span>
                        <span class="title-text-last">Kasih</span>
                    </h1>
                    <h5 class="text-center fw-normal mb-4">telah menggunakan aplikasi kami 😊🙏</h5>
                    <div class="text-center mt-3">Sebelum anda dapat mengakses aplikasi kami, silahkan periksa kotak masuk
                        email anda untuk mengkonfirmasi akun ini. Jika anda tidak menerima email tersebut
                        <form class="d-inline-flex" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0">Tekan disini</button>
                        </form>
                        untuk mengirim ulang email konfirmasi atau hanya
                        <a class="text-decoration-none text-danger lh-1" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar.
                        </a>
                    </div>
                    @if (session('resent'))
                        <div class="alert alert-success my-4" role="alert">
                            {{-- {{ __('A fresh verification link has been sent to your email address.') }} --}}
                            Link verifikasi baru telah dikirim ke alamat email anda.
                        </div>
                    @endif
                </div>
                {{-- <div class="card">
				<div class="card-header">{{ __('Verify Your Email Address') }}</div>

				<div class="card-body">
					@if (session('resent'))
						<div class="alert alert-success" role="alert">
							{{ __('A fresh verification link has been sent to your email address.') }}
						</div>
					@endif

					{{ __('Before proceeding, please check your email for a verification link.') }}
					{{ __('If you did not receive the email') }},
					<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
						@csrf
						<button type="submit"
							class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
					</form>
				</div>
			</div> --}}
            </div>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
