@extends('layouts.app')

@push('style')
    <style>
        .bg-image-vertical {
            position: relative;
            overflow: hidden;
            background-repeat: no-repeat;
            background-position: right center;
            background-size: auto 100%;
        }

        @media (min-width: 1025px) {
            .h-custom-2 {
                height: 100%;
            }
        }
    </style>
@endpush

@section('content')
    <section class="vh-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xxl-4 col-lg-5 col-12 text-black py-5">
                    <div class="px-5 ms-xl-4 mt-3">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo" height="95" width="95">
                        </a>
                        <div class="mt-4">
                            <p class="fs-3">Daftar Sekarang di <br><span
                                    class="fw-semibold">{{ config('app.name', 'Peduli Yatim PENS-ITS') }}</span></p>
                            <p class="mb-0 fs-6">Sebelum dapat mengakses aplikasi kami, silahkan mendaftar akun terlebih
                                dahulu disini! 😊🙏</p>
                        </div>
                    </div>

                    <div class="d-flex align-items-center px-5 ms-xl-4 mt-5">
                        <form method="POST" action="{{ route('register') }}" class="w-100">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label mb-3">Nama <span
                                        class="text-danger">*</span></label>
                                <input type="name" class="form-control py-2 @error('name') is-invalid @enderror"
                                    id="name" placeholder="Jhony Alexander Romeo" autocomplete="name"
                                    value="{{ old('name') }}" name="name">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label mb-3">Email address <span
                                        class="text-danger">*</span></label>
                                <input type="email" class="form-control py-2 @error('email') is-invalid @enderror"
                                    id="email" placeholder="name@example.com" autocomplete="email"
                                    value="{{ old('email') }}" name="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label mb-3">Password <span
                                        class="text-danger">*</span></label>
                                <input type="password" id="password"
                                    class="form-control py-2 @error('password') is-invalid @enderror"
                                    aria-labelledby="passwordHelpBlock" name="password">

                                <div id="passwordHelpBlock" class="form-text">
                                    Kata sandi Anda harus sepanjang 8-20 karakter, mengandung huruf dan angka, dan tidak
                                    boleh berisi spasi, karakter khusus, atau emoji.
                                </div>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="ConfirmPassword" class="form-label mb-3">Ulangi Password <span
                                        class="text-danger">*</span></label>
                                <input type="password" id="ConfirmPassword" class="form-control py-2"
                                    aria-labelledby="passwordHelpBlock" name="password_confirmation">
                            </div>

                            <button class="btn btn-primary w-100 rounded mb-3" type="submit">Daftar</button>

                            <p>Sudah memiliki akun? <a href="{{ route('login') }}">Masuk sekarang!</a></p>

                        </form>
                    </div>

                    {{-- <div class="mt-auto">
                        <p class="mb-0 text-center px-3 fw-semibold">&copy; 2022 Peduli Yatim Pens, org. All right reserved.
                        </p>
                    </div> --}}

                </div>
                <div class="col-xxl-8 col-lg-7 col-12 px-0 d-none d-lg-block sticky-top">
                    <img src="{{ asset('img/asset/loginpage/kids.png') }}" alt="Register image" class="w-100 vh-100"
                        style="object-fit: cover; object-position: center;">
                </div>
            </div>
        </div>
    </section>
@endsection
