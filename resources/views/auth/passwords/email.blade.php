@extends('layouts.app')

@push('style')
    <style>
        .title-text-first {
            font-family: 'Chewy' !important;
            color: #3f3d56;
        }

        .title-text-last {
            font-family: 'Chewy' !important;
            color: #006934;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-8">
                <div class="w-100 my-2 d-flex justify-content-center align-items-center">
                    <div class="rounded-circle neumorph-light p-3">
                        <img src="{{ asset('img/logo.png') }}" alt="Logo" style="height: 20rem; width: auto;">
                    </div>
                </div>
                <div class="w-100 my-3">
                    <h1 class="text-center mb-0t" style="font-size: 5rem;"><span class="title-text-first">Lupa</span>
                        <span class="title-text-last">Kata Sandi</span>
                    </h1>
                    <h5 class="text-center fw-normal mb-4">Harap masukan akun email anda agar sistem dapat mengirimkan pesan
                        untuk merubah password 😊🙏</h5>
                    {{-- <div class="text-center mt-3">Sebelum anda dapat mengakses aplikasi kami, silahkan periksa kotak masuk
                        email anda untuk mengkonfirmasi akun ini. Jika anda tidak menerima email tersebut
                    </div> --}}
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">Email address</label>

                            <div class="col-md-6">
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                <button type="submit" class="btn btn-success">
                                    Kirim link ubah kata sandi
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
