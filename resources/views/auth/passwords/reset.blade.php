@extends('layouts.app')

@push('style')
    <style>
        .title-text-first {
            font-family: 'Chewy' !important;
            color: #3f3d56;
        }

        .title-text-last {
            font-family: 'Chewy' !important;
            color: #006934;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-8">
                <div class="w-100 my-2 d-flex justify-content-center align-items-center">
                    <div class="rounded-circle neumorph-light p-3">
                        <img src="{{ asset('img/logo.png') }}" alt="Logo" style="height: 20rem; width: auto;">
                    </div>
                </div>
                <div class="w-100 my-3">
                    <h1 class="text-center mb-0t" style="font-size: 5rem;"><span class="title-text-first">Ganti</span>
                        <span class="title-text-last">Kata Sandi</span>
                    </h1>
                    <form method="POST" action="{{ route('password.update') }}" class="mt-5">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}" required>
                        <input type="hidden" name="email" value="{{ $email ?? old('email') }}" required>
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Sandi Baru</label>
                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">Konfirmasi Sandi
                                Baru</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4 d-flex justify-content-end my-2">
                                <button type="submit" class="btn btn-success">
                                    Ganti kata sandi
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
