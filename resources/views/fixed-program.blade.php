@extends('layouts.app')

@push('style')
    <style>
        .img-donatur {
            width: 3.5rem;
            height: 3.5rem;
            object-fit: cover;
            object-position: center;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')
    <div class="container">
        <div class="card mb-5">
            <div class="row justify-content-center align-items-center mx-5 p-5">
                <div class="col-8">
                    <h4 class="fw-semibold mb-4 fs-1"><span
                            class="border-success border-bottom border-3">{{ $program->name }}</span></h4>
                    <h6 class="fw-semibold">{{ $program->description }}</h6>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    <img src="{{ asset('img/asset/home/' . $program->photo) }}" alt="img"
                        style="object-position: center; object-fill: contain; width: 17rem; height: auto;">
                </div>
            </div>
        </div>
        <div class="card mb-4 p-5 border-bottom-0 border-start-0 border-end-0 border-4 border-success">
            <h3 class="fw-semibold">Para Donatur ({{ $program->donation_count }})</h3>
            <small class="fw-semibold mb-4">Nama-nama orang-orang baik yang berdonasi</small>
            <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                <img class="img-donatur rounded-circle overflow-hidden me-4"
                    src="{{ asset('img/user/denas-small_1684734725.png') }}" alt="Img"
                    style="object-fit: cover; object-position: center;">
                <div>
                    <h5 class="mb-0 fw-semibold">Denassyah Nurrohman <small class="ms-2 text-secondary fs-6">13 hari yang
                            lalu</small></h5>
                    <small class="fw-semibold">Donasi RP. 10.000</small>
                </div>
            </div>
            <div class="py-4 border-bottom border-success d-flex align-items-center ps-1">
                <img class="img-donatur rounded-circle overflow-hidden me-4" src="{{ asset('img/user/user.png') }}"
                    alt="Img" style="object-fit: cover; object-position: center;">
                <div>
                    <h5 class="mb-0 fw-semibold">Anonymous <small class="ms-2 text-secondary fs-6">13 hari yang
                            lalu</small></h5>
                    <small class="fw-semibold">Donasi RP. 10.000</small>
                </div>
            </div>
            {{-- Carbon\Carbon::parse($item->end_at)->diffForHumans() --}}
            {{-- <h6 class="fw-semibold mt-3">Belum ada Donatur saat ini</h6> --}}
        </div>
    </div>
    <div class="sticky-bottom d-flex align-items-center justify-content-center card">
        <a href="{{ route('create-donation', $program->id) }}"
            class="btn btn-success px-5 py-2 rounded-pill my-3 fw-semibold fs-5 text-decoration-none text-white"
            style="width: 40rem;">
            Donasi Sekarang!
        </a>
    </div>
@endsection
