@extends('layouts.app')

@push('style')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables1.13.4.min.css') }}">
    <style>
        .accordion-button {
            background-color: #E8F6ED;
            color: #007139;
        }
    </style>
@endpush

@section('content')
    @include('layouts.navbar')
    {{-- Start Content Section --}}
    <div class="container vw-100 min-vh-100">
        {{-- Start Header Section --}}
        <section class="w-100 position-relative overflow-hidden header">
            <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle"
                src="{{ asset('img/asset/home/bg atas.png') }}" alt="Background Img" style="object-fit: cover;">
            <div
                class="position-absolute top-50 start-50 translate-middle px-5 row justify-content-center align-items-center w-100">
                <div class="col-6">
                    <h3 class="fw-semibold">
                        <span class="border-success border-3 border-bottom">
                            Laporan Keuangan Yayasan
                        </span>
                    </h3>
                    <h6 class="mb-0 mt-4 fw-semibold">
                        Di sini, kami membagikan laporan keuangan terkini Yayasan Peduli Yatim sebagai wujud transparansi
                        dan akuntabilitas kami terhadap penggunaan dana yang kami terima. Halaman ini menyajikan informasi
                        yang lengkap dan jelas mengenai kondisi keuangan Yayasan.
                    </h6>
                </div>
                <div class="col-2 offset-1 px-0">
                    <img class="w-100 h-100" src="{{ asset('img/logo.png') }}" alt="logo" style="object-fit: contain;">
                </div>
                <div class="col-12">

                </div>
            </div>
        </section>
        {{-- End Header Section --}}

        {{-- Start List Donation History Section --}}
        <div class="card border-0 mt-4">
            <div class="px-4 pt-4">
                <h5 class="fw-semibold mb-3 mx-2">
                    Filter Laporan
                </h5>
                <div class="d-flex flex-wrap justify-content-between align-items-center">
                    <div class="d-flex flex-wrap">
                        <button class="btn btn-success btn-sm px-4 rounded-pill mx-2" type="button"
                            data-bs-toggle="collapse" data-bs-target="#list-6" aria-expanded="false"
                            aria-controls="list-6">Laporan Pemasukan</button>
                        <button class="btn btn-outline-success btn-sm px-4 rounded-pill mx-2" type="button"
                            data-bs-toggle="collapse" data-bs-target="#list-6" aria-expanded="false"
                            aria-controls="list-6">Laporan Pengeluaran</button>
                    </div>
                    <h5 class="mb-0">Laporan Keuangan Bulan: <span class="fw-semibold">Mei 2023</span></h5>
                </div>

                <div class="row py-4 px-3" id="laporan_pemasukan">
                    <div class="accordion px-0" id="accordionFlushExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false"
                                    aria-controls="flush-collapseOne">
                                    Infaq Anak Yatim Piatu dan Dhuafa
                                    <span class="ps-3">
                                        ( Rp. 50.730.500,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                    aria-controls="flush-collapseTwo">
                                    Infaq Zakat
                                    <span class="ps-3">
                                        ( Rp. 10.110.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Waqaf Tanah
                                    <span class="ps-3">
                                        ( Rp. 230.500,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Infaq Swakelola Yayasan
                                    <span class="ps-3">
                                        ( Rp. 500.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Bantuan Modal Usaha Yayasan
                                    <span class="ps-3">
                                        ( Rp. 50.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseFive" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Program Lainnya
                                    <span class="ps-3">
                                        ( Rp. 103.475.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseSix" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Program</th>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Pembagunan Masjid At-Thoriq</b></td>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Pembelian 1000 Al-Quran</b></td>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Berbagi Takjil Bersama</b></td>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row py-4 px-3" id="laporan_pengeluaran">
                    <div class="accordion px-0" id="accordionFlushExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false"
                                    aria-controls="flush-collapseOne">
                                    Infaq Anak Yatim Piatu dan Dhuafa
                                    <span class="ps-3">
                                        ( Rp. 50.730.500,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                    aria-controls="flush-collapseTwo">
                                    Infaq Zakat
                                    <span class="ps-3">
                                        ( Rp. 10.110.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Waqaf Tanah
                                    <span class="ps-3">
                                        ( Rp. 230.500,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Infaq Swakelola Yayasan
                                    <span class="ps-3">
                                        ( Rp. 500.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Bantuan Modal Usaha Yayasan
                                    <span class="ps-3">
                                        ( Rp. 50.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseFive" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    Program Lainnya
                                    <span class="ps-3">
                                        ( Rp. 103.475.000,- )
                                    </span>
                                </button>
                            </h2>
                            <div id="flush-collapseSix" class="accordion-collapse collapse"
                                data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <table class="display table table-striped py-4">
                                        <thead>
                                            <tr>
                                                <th>Nama Program</th>
                                                <th>Nama Donatur</th>
                                                <th>Nominal Donasi</th>
                                                <th>Tanggal Donasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Pembagunan Masjid At-Thoriq</b></td>
                                                <td><b>Denassyah Nurrohman</b></td>
                                                <td>Rp. 100.000</td>
                                                <td>6 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Pembelian 1000 Al-Quran</b></td>
                                                <td><b>Bayu Hidayah Nur Muhammad</b></td>
                                                <td>Rp. 500.000</td>
                                                <td>1 Juni 2023</td>
                                            </tr>
                                            <tr>
                                                <td><b>Berbagi Takjil Bersama</b></td>
                                                <td><b>Mukhlis Bara Pamungkas</b></td>
                                                <td>Rp. 50.000</td>
                                                <td>30 Juni 2023</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End List Donation History Section --}}
    </div>
    {{-- End Content Section --}}
@endsection

@push('script')
    <script src="{{ asset('js/jquery-3.7.0.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables1.13.4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.table').DataTable({
                "language": {
                    "emptyTable": "Maaf, tidak ada data untuk laporan ini."
                }
            });
        });
    </script>
@endpush
