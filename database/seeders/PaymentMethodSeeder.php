<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		$datas = [
			//!-- TRANSFER BANK --
			[
				'name'                       => 'bca',
				'logo'                       => 'bca.png',
				'fee'                        => 4000,
				'fee_type'                   => 'flat_ppn',
				'payment_method_category_id' => 1,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'bni',
				'logo'                       => 'bni.png',
				'fee'                        => 4000,
				'fee_type'                   => 'flat_ppn',
				'payment_method_category_id' => 1,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'bri',
				'logo'                       => 'bri.png',
				'fee'                        => 4000,
				'fee_type'                   => 'flat_ppn',
				'payment_method_category_id' => 1,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'mandiri',
				'logo'                       => 'mandiri.png',
				'fee'                        => 4000,
				'fee_type'                   => 'flat_ppn',
				'payment_method_category_id' => 1,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'permata',
				'logo'                       => 'permata.png',
				'fee'                        => 4000,
				'fee_type'                   => 'flat_ppn',
				'payment_method_category_id' => 1,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'gopay',
				'logo'                       => 'gopay.png',
				'fee'                        => 2,
				'fee_type'                   => 'percentage',
				'payment_method_category_id' => 2,
				'is_active'					 => 1,
			],
			[
				'name'                       => 'shopeepay',
				'logo'                       => 'shopeepay.png',
				'fee'                        => 2,
				'fee_type'                   => 'percentage',
				'payment_method_category_id' => 2,
				'is_active'					 => 0,
			],
			[
				'name'                       => 'qris',
				'logo'                       => 'qris.png',
				'fee'                        => 0.7,
				'fee_type'                   => 'percentage',
				'payment_method_category_id' => 2,
				'is_active'					 => 0,
			],
		];

		PaymentMethod::insert($datas);
	}
}
