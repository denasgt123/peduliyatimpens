<?php

namespace Database\Seeders;

use App\Models\DonateProgram;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DonateProgramSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		$datas_temp = [
			[
				'slug'       			=> Str::slug('Pembagunan Masjid At-Thoriq'),
				'name'       			=> 'Pembagunan Masjid At-Thoriq',
				'photo'       			=> 'pembangunan masjid.jpg',
				'program_type'			=> 'temporary',
				'description'			=> 'Pembangunan Masjid At-Thoriq adalah sebuah program donasi yang bertujuan untuk mendukung dan memfasilitasi pembangunan sebuah masjid yang bernama Masjid At-Thoriq. Program ini mengajak individu, kelompok, dan masyarakat luas untuk berpartisipasi dalam membangun tempat ibadah yang menjadi pusat kegiatan keagamaan dan sosial bagi komunitas setempat.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
				'end_at'				=> Carbon::now()->addYears(rand(1, 10)),
				'created_at'			=> Carbon::now()->subYears(rand(1, 10)),
				'updated_at'			=> Carbon::now()->subYears(rand(1, 10)),
			],
			[
				'slug'       			=> Str::slug('Pembelian 1000 Al-Quran'),
				'name'       			=> 'Pembelian 1000 Al-Quran',
				'photo'       			=> 'al-quran.jpg',
				'program_type'			=> 'temporary',
				'description'			=> 'Program donasi Pembelian 1000 Al-Quran adalah sebuah inisiatif yang bertujuan untuk mengumpulkan dana guna membeli dan mendistribusikan 1000 Al-Quran kepada individu dan komunitas yang membutuhkan. Program ini dirancang untuk memfasilitasi akses dan penyebaran kitab suci Al-Quran kepada mereka yang tidak mampu atau memiliki keterbatasan dalam memperoleh salinan Al-Quran.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
				'end_at'				=> Carbon::now()->addYears(rand(1, 10)),
				'created_at'			=> Carbon::now()->subYears(rand(1, 10)),
				'updated_at'			=> Carbon::now()->subYears(rand(1, 10)),
			],
			[
				'slug'       			=> Str::slug('Bantuan Korban Banjir Timor Leste'),
				'name'       			=> 'Bantuan Korban Banjir Timor Leste',
				'photo'       			=> 'banjir.jpg',
				'program_type'			=> 'temporary',
				'description'			=> 'Program donasi Bantuan Korban Banjir Timor Leste adalah sebuah inisiatif yang ditujukan untuk memberikan bantuan dan dukungan kepada korban bencana banjir di Timor Leste. Program ini bertujuan untuk membantu masyarakat yang terdampak banjir dalam memulihkan diri, menyediakan bantuan darurat, dan membantu membangun kembali kehidupan mereka setelah bencana.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
				'end_at'				=> Carbon::now()->addYears(rand(1, 10)),
				'created_at'			=> Carbon::now()->subYears(rand(1, 10)),
				'updated_at'			=> Carbon::now()->subYears(rand(1, 10)),
			],
			[
				'slug'       			=> Str::slug('Bantuan Korban Perang Palestina'),
				'name'       			=> 'Bantuan Korban Perang Palestina',
				'photo'       			=> 'perang palestina.jpg',
				'program_type'			=> 'temporary',
				'description'			=> 'Program donasi Bantuan Korban Perang Palestina adalah sebuah inisiatif yang bertujuan untuk memberikan bantuan dan dukungan kepada masyarakat Palestina yang terdampak konflik dan perang di wilayah mereka. Program ini dirancang untuk membantu korban perang Palestina dalam memenuhi kebutuhan dasar mereka, menyediakan bantuan medis, pendidikan, perlindungan, serta membantu memulihkan kehidupan mereka yang terpengaruh oleh konflik yang berkepanjangan.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
				'end_at'				=> Carbon::now()->addYears(rand(1, 10)),
				'created_at'			=> Carbon::now()->subYears(rand(1, 10)),
				'updated_at'			=> Carbon::now()->subYears(rand(1, 10)),
			],
			[
				'slug'       			=> Str::slug('Berbagi Takjil Bersama'),
				'name'       			=> 'Berbagi Takjil Bersama',
				'photo'       			=> 'takjil.jpg',
				'program_type'			=> 'temporary',
				'description'			=> 'Program donasi "Berbagi Takjil Bersama" adalah sebuah inisiatif yang bertujuan untuk mendorong solidaritas sosial dan kepedulian terhadap sesama dalam bulan Ramadan. Program ini mengajak individu, kelompok, dan komunitas untuk berpartisipasi dalam berbagi takjil kepada masyarakat yang membutuhkan atau yang sedang menjalankan ibadah puasa. Melalui program donasi "Berbagi Takjil Bersama", setiap donatur memiliki kesempatan untuk berbagi kebahagiaan dan meringankan beban mereka yang membutuhkan selama bulan Ramadan.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
				'end_at'				=> Carbon::now()->addYears(rand(1, 10)),
				'created_at'			=> Carbon::now()->subYears(rand(1, 10)),
				'updated_at'			=> Carbon::now()->subYears(rand(1, 10)),
			],
		];

		$datas = [
			[
				'slug'       			=> Str::slug('Infaq Anak Yatim Piatu dan Dhuafa'),
				'name'       			=> 'Infaq Anak Yatim Piatu dan Dhuafa',
				'photo'       			=> 'Infaq Yatim.svg',
				'program_type'			=> 'permanent',
				'description'			=> 'Infaq Anak Yatim Piatu dan Dhuafa adalah program donasi yang bertujuan untuk memberikan dukungan dan bantuan kepada anak yatim piatu dan individu yang kurang mampu. Program ini bertujuan untuk meringankan beban keuangan mereka dan meningkatkan kualitas hidup.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
			],
			[
				'slug'       			=> Str::slug('Infaq Zakat'),
				'name'       			=> 'Infaq Zakat',
				'photo'       			=> 'Zakat.svg',
				'program_type'			=> 'permanent',
				'description'			=> 'Infaq Zakat adalah sebuah program donasi yang bertujuan untuk mengumpulkan dana dari masyarakat guna membantu mereka yang membutuhkan. Program ini dirancang untuk memudahkan para individu yang ingin berinfaq dan mengeluarkan zakat secara praktis, aman, dan transparan.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
			],
			[
				'slug'       			=> Str::slug('Waqaf Tanah'),
				'name'       			=> 'Waqaf Tanah',
				'photo'       			=> 'Waqaf.svg',
				'program_type'			=> 'permanent',
				'description'			=> 'Waqaf Tanah adalah sebuah program donasi yang bertujuan untuk mengumpulkan donasi dari masyarakat untuk pembelian & pengelolaan tanah guna dimanfaatkan secara produktif dan memberikan manfaat jangka panjang bagi masyarakat.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
			],
			[
				'slug'       			=> Str::slug('Infaq Swakelola Yayasan'),
				'name'       			=> 'Infaq Swakelola Yayasan',
				'photo'       			=> 'Swakelola.svg',
				'program_type'			=> 'permanent',
				'description'			=> 'Infaq Swakelola Yayasan adalah sebuah program donasi yang dirancang khusus untuk mendukung yayasan dalam menjalankan kegiatan sosial, edukasi, dan kemanusiaan. Program ini memungkinkan individu, perusahaan, atau lembaga untuk memberikan sumbangan finansial yang langsung dikelola dan digunakan oleh yayasan untuk kepentingan mereka.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
			],
			[
				'slug'       			=> Str::slug('Bantuan Modal Usaha Yayasan'),
				'name'       			=> 'Bantuan Modal Usaha Yayasan',
				'photo'       			=> 'Modal.svg',
				'program_type'			=> 'permanent',
				'description'			=> 'Bantuan Modal Usaha Yayasan adalah sebuah program donasi yang bertujuan untuk memberikan dukungan finansial kepada individu atau kelompok yang membutuhkan modal usaha. Program ini dirancang untuk membantu masyarakat yang ingin memulai atau mengembangkan usaha mereka, namun menghadapi kendala dalam memperoleh modal yang diperlukan.',
				'donate_goal'			=> rand(1000000, 10000000),
				'current_donate_amount'	=> rand(1000000, 9000000),
				'donation_count'		=> rand(10, 100),
			]
		];

		DonateProgram::insert($datas);
		DonateProgram::insert($datas_temp);
	}
}
