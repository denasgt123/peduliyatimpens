<?php

namespace Database\Seeders;

use App\Models\PaymentMethodCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentMethodCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'name' => 'Transfer Bank',
            ],
            [
                'name' => 'E-Wallets',
            ],
            [
                'name' => 'Kartu Kredit',
            ],
            [
                'name' => 'Bayar Via Counter',
            ],
            [
                'name' => 'Direct Debit',
            ],
            [
                'name' => 'Cardless Credit',
            ],
        ];

        PaymentMethodCategory::insert($datas);
    }
}
