<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		$datas = [];
		$date = Carbon::create(2005, 1, 1);
		for ($i = 1; $i <= 10; $i++) {
			$datas[] = [
				'name'				=> "User$i",
				'bio'				=> "I love to be here as User$i",
				'email'				=> "user$i@user$i.com",
				'phone'				=> mt_rand(100000000000, 999999999999),
				'birth_date' 		=> $date->subYears(rand(1, 10)),
				'whatsapp'			=> rand(0, 1),
				'hide_me'			=> rand(0, 1),
				'email_verified_at' => Carbon::now(),
				'password'          => bcrypt('qweqweqwe'),
			];
		}
		User::insert($datas);
	}
}
