<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('donate_program_histories', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('program_id');
			$table->unsignedBigInteger('payment_method_id')->nullable();
			$table->string('transaction_id')->unique();
			$table->longText('qris_payment')->nullable();
			$table->unsignedBigInteger('amount_donation');
			$table->string('status');
			$table->string('snap_id')->nullable();
			$table->boolean('is_anonim')->default(0);
			// $table->dateTime('donate_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::dropIfExists('donate_program_histories');
	}
};
