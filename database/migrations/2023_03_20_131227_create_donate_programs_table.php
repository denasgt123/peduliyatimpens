<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('donate_programs', function (Blueprint $table) {
			$table->id();
			$table->string('slug');
			$table->string('name');
			$table->string('photo')->nullable();
			$table->enum('program_type', ['permanent', 'temporary'])->default('temporary');
			// $table->longText('description')->default('Tidak ada deskripsi untuk program donasi ini');
			$table->longText('description');
			$table->unsignedBigInteger('donate_goal')->default(0);
			$table->unsignedBigInteger('current_donate_amount')->default(0);
			$table->unsignedBigInteger('donation_count')->default(0);
			$table->dateTime('end_at')->nullable();
			// $table->string('contact_person')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::dropIfExists('donate_programs');
	}
};
